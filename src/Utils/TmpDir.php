<?php

namespace Libriciel\LibActes\Utils;

class TmpDir {

	private $tmp_dir_list = array();

	public function getTmpDir(){
		$tmp_dir = sys_get_temp_dir()."/".mt_rand(0,mt_getrandmax());
		mkdir($tmp_dir);
		$this->tmp_dir_list[] = $tmp_dir;
		return $tmp_dir;

	}

	public function destruct() {
		foreach($this->tmp_dir_list as $tmp_dir){
			$command = "rm -r $tmp_dir";
			exec($command);
		}
	}
}