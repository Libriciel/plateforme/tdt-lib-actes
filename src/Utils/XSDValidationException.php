<?php

namespace Libriciel\LibActes\Utils;

class XSDValidationException extends \Exception {

    private $validation_errors = array();

    public function __construct(array $validation_errors) {
        parent::__construct("Le document n'est pas valide");
        $this->setValidationError($validation_errors);
    }

    public function setValidationError(array $validation_errors){
        $this->validation_errors = $validation_errors;
    }

    public function getValidationErrors(){
        return $this->validation_errors;
    }

    public function displayValidationErrors(){
        $result = [];
        foreach($this->getValidationErrors() as $error){
            $result[] = "Level : {$error->level} - ".
                "Code : {$error->code} - ".
                "Line : {$error->line} - ".
                "Column : {$error->column} - ".
                "Message : {$error->message}";
        }
        return $result;
    }

}