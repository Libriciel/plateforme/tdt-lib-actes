<?php

namespace Libriciel\LibActes\Utils;

class XpathPropertiesValuesMapper {

    /**
     * @param array $properties_mapping un tableau de clé => value
     *              avec la clé une expression xpath et value le nom d'un propriété d'un objet
     * @param object $object l'objet à utiliser pour récupérer les proriétés
     * @return array $xpath_mapping le tableau en entré avec comme valeur les propriétés de l'objet
     */
    public function getMappingValues(array $properties_mapping, object $object): array
    {
        $properties_mapping = $this->expendArrayProperties($properties_mapping,$object);
        $properties_mapping = $this->removeEmptyProperties($properties_mapping,$object);

        // $properties_mapping :
        // ['racine/test/@actes:Departement' => "var_name",'racine/test/@actes:Classification' => "var_name2"]
        $xpathMapping = array();

        foreach($properties_mapping as $xpath => $propertie){
            // ex $xpath = racine/test/@actes:Departement
            // $propertie = "var_name"

            $xpath = ltrim($xpath,"/");
            $xpathMappingCurrentLevel = &$xpathMapping;
            $xpath_part = false;
            foreach(explode("/",$xpath) as $xpath_part){// explode("/",$xpath) donne ["racine","test,"@actes:Departement"]
                // On va compléter $xpathMapping pour donner un multidimensional array
                if (empty($xpathMappingCurrentLevel[$xpath_part])){ // Si la clé $xpath_part n'existe pas au niveau courant,
                    $xpathMappingCurrentLevel[$xpath_part] = [];    // on la crée
                }
                $xpathMappingPreviousLevel = &$xpathMappingCurrentLevel;             // On garde en mémoire le niveau courant pour y ajouter les valeurs
                $xpathMappingCurrentLevel = &$xpathMappingCurrentLevel[$xpath_part]; // On monte d'un niveau dans le tableau
            }
            // On ajoute la valeur de la propriété au tableau $xpathMappingPreviousLevel gardé en mémoire
            if (is_array($propertie)){
                $xpathMappingPreviousLevel[$xpath_part] = $object->{$propertie[0]}[$propertie[1]];
            } else {
                $xpathMappingPreviousLevel[$xpath_part] = $object->$propertie;
            }

        }

        //$xpathMapping
        // ['racine'=>[
        //      'test'=>
        //          ['@actes:Departement'=>"var_value"],
        //          ['@actes:Classification'=>"var_value2"],
        //  ]
        //]
        return $xpathMapping;
    }

    private function expendArrayProperties($properties_mapping,$object) {
        foreach ($properties_mapping as $xpath => $properties) {
            if (is_array($object->$properties)) {
                foreach ($object->$properties as $i => $value) {
                    if ($i == 0) {
                        $new_xpath = preg_replace('#\[\]#', "", $xpath);
                    } else {
                        $num = $i + 1;
                        $new_xpath = preg_replace('#\[\]#', "[$num]", $xpath);
                    }

                    $properties_mapping[$new_xpath] = array($properties, $i);
                }
                unset($properties_mapping[$xpath]);
            }
        }
        return $properties_mapping;
    }

    private function removeEmptyProperties(array $properties_mapping, $object): array
    {
        foreach($properties_mapping as $xpath => $propertie) {
            if (! is_array($propertie) && ! isset($object->$propertie)){
                unset($properties_mapping[$xpath]);
            }
        }
        return $properties_mapping;
    }
}