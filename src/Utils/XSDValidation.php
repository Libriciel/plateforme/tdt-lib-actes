<?php

namespace Libriciel\LibActes\Utils;

class XSDValidation {

    public function validateFile($filename,$xsd_path){
        return $this->validate(file_get_contents($filename),$xsd_path);
    }

	public function validate($xml_content, $xsd_path){
		libxml_use_internal_errors(true);
		libxml_clear_errors();
		$dom = new \DOMDocument();
		$dom->loadXML($xml_content);
		$result = $dom->schemaValidate($xsd_path);
		$last_errors = libxml_get_errors();
		libxml_clear_errors();
		if( $last_errors) {
            throw new XSDValidationException($last_errors);
        }
		return $result;
	}

}