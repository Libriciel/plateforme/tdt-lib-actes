<?php

namespace Libriciel\LibActes\Utils;

class XMLGenerator {

	const XSD_PREFIX = "xsd";
	const XSD_NS = "http://www.w3.org/2001/XMLSchema";

	private $target_namespace;
	private $target_prefix;

	/** @var  \SimpleXMLElement */
	private $simpleXMLXSD;

	/** @var  \DOMDocument */
	private $documentResult;

	private $xsd_content;
	private $xpath_mapping;

    /**
     * @param $xsd_content
     * @param array $xpath_mapping
     * @return string
     * @throws \Exception
     */
	public function createXMLfromXSD($xsd_content, array $xpath_mapping){
        $this->xsd_content = $xsd_content;
        $this->xpath_mapping = $xpath_mapping;

		$node = $this->getNodeElement($this->getRootElement());
		$this->documentResult = new \DOMDocument();
		$this->documentResult->preserveWhiteSpace = false;
		$this->documentResult->formatOutput = true;
		$this->traiterElement($node,$this->documentResult);
		return $this->documentResult->saveXML();
	}

    protected function getRootElement(){
        $root = array_keys($this->xpath_mapping)[0];
        $result = explode(':', $root);
        return $result[1];
    }

    protected function getSimpleXMLXSD(){
		if (! $this->simpleXMLXSD) {
			$this->simpleXMLXSD = simplexml_load_string($this->xsd_content);
			$this->simpleXMLXSD->registerXPathNamespace(self::XSD_PREFIX, self::XSD_NS);
			$this->target_namespace = (string) $this->simpleXMLXSD->attributes()->{'targetNamespace'};
			$namespace_list = array_flip($this->simpleXMLXSD->getDocNamespaces());
			$this->target_prefix = $namespace_list[$this->target_namespace];
			$this->simpleXMLXSD->registerXPathNamespace($this->target_prefix, $this->target_namespace);
		}
		return $this->simpleXMLXSD;
	}

    /**
     * @param $xpath_expression
     * @return \SimpleXMLElement
     * @throws \Exception
     */
	protected function searchUnique($xpath_expression){
		$xml = $this->getSimpleXMLXSD();
		$node_list = $xml->xpath($xpath_expression);

		if (count($node_list) == 0){
			throw new \Exception("Impossible de trouver l'élement pour le chemin $xpath_expression");
		}
		return $node_list[0];
	}

    /**
     * @param $element
     * @return \SimpleXMLElement
     * @throws \Exception
     */
	protected function getNodeElement($element){
		return $this->searchUnique("//xsd:element[@name='$element']");
	}

    /**
     * @param $type_name
     * @return \SimpleXMLElement
     * @throws \Exception
     */
	protected function getComplexType($type_name){
		return $this->searchUnique("//xsd:complexType[@name='$type_name']");
	}

    /**
     * @param $type_name
     * @return \SimpleXMLElement
     * @throws \Exception
     */
	protected function getSimpleType($type_name){
		return $this->searchUnique("//xsd:simpleType[@name='$type_name']");
	}

	protected function walk(\SimpleXMLElement $element,\DOMNode $domNodeResult){
		$children_list = $element->children(self::XSD_NS);
		/** @var \SimpleXMLElement $node */
		foreach($children_list as $node){
			$node_name = $node->getName();
			$function = "traiter".ucfirst($node_name);
			$this->$function($node,$domNodeResult);
		}
	}

    /**
     * @param \SimpleXMLElement $element
     * @param \DOMNode $domNodeResult
     * @throws \Exception
     */
	protected function traiterElement(\SimpleXMLElement $element,\DOMNode $domNodeResult){
		$element_ref = (string) $element->attributes()->{'ref'};
		if ($element_ref){
			$element_name = explode(':',$element_ref)[1];
			$element = $this->getNodeElement($element_name);
			$this->traiterElement($element, $domNodeResult);
			return;
		}
		$element_name = (string)$element->attributes()->{'name'};

		$nb_occurs  = $this->getElementNbOccurs($domNodeResult,$element_name);
		for($i=0; $i < $nb_occurs; $i++) {

			$new_element = $this->documentResult->createElementNS($this->target_namespace, "{$this->target_prefix}:$element_name");

			$domNodeResult->appendChild($new_element);

			if (isset($element->attributes()->{'type'})) {
				$type = (string)$element->attributes()->{'type'};
				list($ns, $type_name) = explode(':', $type);
				if ($ns == $this->target_prefix) {
					try {
						$complexType = $this->getComplexType($type_name);
						$this->traiterComplexType($complexType, $new_element);
					} catch (\Exception $e) {
						try {
							$simpleType = $this->getSimpleType($type_name);
							$this->traiterSimpleType($simpleType, $new_element);
						} catch (\Exception $e2){
							throw $e;
						}
					}
				} else {
					//$new_element->nodeValue = $this->getNodeValue($new_element);
                    $new_element->appendChild($this->documentResult->createTextNode($this->getNodeValue($new_element)));
				}
			}
			$this->walk($element, $new_element);
		}
	}

	protected function traiterAnnotation(){}

	protected function traiterComplexType(\SimpleXMLElement $element, \DOMNode $domNodeResult){
		$this->walk($element,$domNodeResult);
	}

	protected function traiterSequence(\SimpleXMLElement $element,\DOMNode $domNodeResult){
		$this->walk($element,$domNodeResult);
	}

	protected function traiterAttribute(\SimpleXMLElement $element,\DOMElement $domNodeResult){
		if ($element->attributes()->{'name'}) {
			$attribute_name = "{$this->target_prefix}:".strval($element->attributes()->{'name'});
			$namespace = $this->target_namespace;
		} else {
			/** @var \SimpleXMLElement $ref */
			$attribute_name = $element->attributes()->{'ref'};
			$l = explode(":",$attribute_name);

			$prefix = $l[0];
			$namespace_list = $this->simpleXMLXSD->getDocNamespaces();
			$namespace =  $namespace_list[$prefix];
		}

		$attributeNode = $this->documentResult->createAttributeNS($namespace, $attribute_name);
		$domNodeResult->appendChild($attributeNode);

        $attributeNode->appendChild($this->documentResult->createTextNode($this->getNodeValue($attributeNode)));

	}

	protected function traiterRestriction(){}

	protected function traiterChoice(\SimpleXMLElement $element,\DOMNode $domNodeResult){
		$this->walk($element,$domNodeResult);
	}

	protected function traiterSimpleType(\SimpleXMLElement $element,\DOMNode $domNodeResult){
        $domNodeResult->appendChild($this->documentResult->createTextNode($this->getNodeValue($domNodeResult)));
		//$domNodeResult->nodeValue = $this->getNodeValue($domNodeResult);
		$this->walk($element,$domNodeResult);
	}

	protected function traiterComplexContent(\SimpleXMLElement $element,\DOMNode $domNodeResult){
		$this->walk($element,$domNodeResult);
	}

    /**
     * @param \SimpleXMLElement $element
     * @param \DOMNode $domNodeResult
     * @throws \Exception
     */
	protected function traiterExtension(\SimpleXMLElement $element,\DOMNode $domNodeResult){
		$base_name = $element->attributes()->{'base'};
		$ref = explode(":",$base_name)[1];
		$new_element = $this->getComplexType($ref);
		$this->traiterComplexType($new_element, $domNodeResult);

		$this->walk($element,$domNodeResult);
	}

    protected function getValueFromMapping($xpath){
        $xpath = trim($xpath);
        $result = $this->xpath_mapping;

        $path_parts = explode("/",ltrim($xpath,"/"));

        foreach($path_parts as $part){
            if (isset($result[$part])) {
                $result = $result[$part];
            } else {
                return false;
            }
        }

        return $result;
    }

    protected function getNodeValue(\DOMNode $node){
        return $this->getValueFromMapping($node->getNodePath()) ;
    }

    protected function getElementNbOccurs(\DOMNode $node ,$element_name){
        $element_name = "{$this->target_prefix}:$element_name";

        $xpath = $node->getNodePath()."/$element_name";
        if( ! $this->getValueFromMapping($xpath)){
            return 0;
        }
        $result = $this->getValueFromMapping($node->getNodePath());
        if (!is_array($result)){
            return 1;
        }
        $nb = 0;
        foreach(array_keys($result) as $node_name ){
            $node_name = strstr($node_name,'[',true)?:$node_name;
            if ($node_name == $element_name) {
                $nb++;
            }
        }

        return $nb;
    }
}