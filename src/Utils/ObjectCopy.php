<?php

namespace Libriciel\LibActes\Utils;

class ObjectCopy {

    public function copy($from, $to){
        foreach (get_object_vars($from) as $key => $value) {
            if (isset($from->$key)) {
                $to->$key = $from->$key;
            }
        }
    }
}