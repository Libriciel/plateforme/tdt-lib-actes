<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\FichierXML\FichierXML;

class TransmissionGenerator {

    public function generateFiles(
        FichierXML $messageMetierActes,
        $output_directory
    ){

        $actesXML = new ActesXML();
        $xml = $actesXML->getXML($messageMetierActes);

        $messageMetierFilename = new MessageMetierFilename();
        $message_metier_filename = $messageMetierFilename->getNomMessageMetier($messageMetierActes);
        $message_metier_filepath = "$output_directory/$message_metier_filename";

        file_put_contents(
            $message_metier_filepath,
            $xml
        );

        $file_list = $actesXML->getFileListe($messageMetierActes);

        foreach($file_list as $filekey => $filepath){
            copy(
                $filepath,
                $output_directory."/".$filekey
            );
        }

        return $message_metier_filepath;
    }

    public function getTransmissionData($message_metier_path, $validate_format = true){
        $actesXML = new ActesXML();
        $fichierXML = $actesXML->getDataFromFile($message_metier_path);
        foreach($fichierXML->getFileList() as $filekey){
            if (is_array($fichierXML->$filekey)){
                foreach($fichierXML->$filekey as $i => $filepath){
                    $fkey = & $fichierXML->$filekey;
                    $filekey_tmp = $fichierXML->$filekey;
                    $fkey[$i] = dirname($message_metier_path)."/".$filekey_tmp[$i];
                    if ($validate_format) {
                        $this->validateFileFormat($fkey[$i]);
                    }
                }
            } else {
                $fichierXML->$filekey = dirname($message_metier_path)."/".$fichierXML->$filekey;
                if ($validate_format) {
                    $this->validateFileFormat($fichierXML->$filekey);
                }
            }
        }
        return $fichierXML;
    }

    public function validateFileFormat($filepath){
        if ( ! file_exists($filepath)){
            throw new \Exception("Le fichier ".basename($filepath)." est référencé, mais il n'est pas présent dans l'archive");
        }
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime_type = finfo_file($finfo, $filepath);
        finfo_close($finfo);
        if (! in_array($mime_type,array('application/pdf','image/png','image/jpeg','application/xml', 'text/xml'))){
            throw new \Exception("Le format $mime_type du fichier ".basename($filepath)." n'est pas autorisé");
        }
    }

}