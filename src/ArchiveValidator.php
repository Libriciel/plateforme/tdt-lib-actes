<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\FichierXML\FichierXML;
use Libriciel\LibActes\FichierXML\MessageMetierActes;
use Libriciel\LibActes\Utils\TmpDir;

class ArchiveValidator {

    const ARCHIVE_MAX_SIZE_IN_BYTES = 157286400; //150 * 1024 * 1024

    private $id_tdt;
    private $id_application;
    private $is_mandatory_typologie;

    private $typologie_by_nature = [];

    public function __construct($id_tdt,$id_application = 'EACT',$is_mandatory_typologie = false) {
        $this->id_tdt = $id_tdt;
        $this->id_application = $id_application;
        $this->is_mandatory_typologie = $is_mandatory_typologie;
    }

    /**
     *
     * Permet d'appliquer la nouvelle notice de la DGCL sur l'obligation de typer les pièces par nature
     * @param array $typologie_by_nature
     */
    public function setValidationTypologieByNature(array $typologie_by_nature){
        $this->typologie_by_nature = $typologie_by_nature;
    }

    /**
     * @param $archive_filepath
     * @return bool
     * @throws \Exception
     */
    public function validate($archive_filepath,array $authorized_nature = [], array $authorized_typologie = []){

		//TODO On passe une liste de nature et de type pj qu'on vérifie après

        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        try {
            $this->validateArchiveThrow($archive_filepath,$tmp_dir,$authorized_nature,$authorized_typologie);
        } catch(\Exception $e){
            $tmpDir->destruct();
            throw $e;
        }
        $tmpDir->destruct();
        return true;
    }

    /**
     * @param $archive_filepath
     * @param $tmp_dir
     * @throws \Exception
     */
    private function validateArchiveThrow($archive_filepath,$tmp_dir,$authorized_nature,$authorized_typologie){
        $archive = new Archive();

        $archiveData = $archive->getArchiveDataFromTarball($archive_filepath,$tmp_dir);

        //NORME5 :  Nom de l'archive
        $this->validateRightTdt($archiveData);

		//NORME8 :  Nom de l'application
		$this->validateRightApplication($archiveData);


        //NORME7 : Pas plus de 150 Mo
        $this->validateArchiveSize($archive_filepath);


        //FONCT09 :
        $this->validateActesBudgetaire($archiveData);

        foreach($archiveData->fichierXML as $fichierXML){
        	$fichierXML->validate();
		}

		//NORME 11 : nom des fichiers OK
		$this->validateFichierFilename($archiveData,$authorized_nature,$authorized_typologie);


        // NORME 12 : numéro SIREN ok
        // Vérifier numéro siren nom message == contenu message
        // Vérifier extension fichier
    }

    /**
     * @param ArchiveData $archiveData
     * @throws \Exception
     */
    private function validateRightTdt(ArchiveData $archiveData){
        if ($archiveData->id_tdt != $this->id_tdt){
            throw new \Exception(
                "L'identifiant du tdt de l'archive {$archiveData->id_tdt} ne correspond pas à l'identifiant courant {$this->id_tdt}"
            );
        }
    }

	/**
	 * @param ArchiveData $archiveData
	 * @throws \Exception
	 */
	private function validateRightApplication(ArchiveData $archiveData){
		if ($archiveData->id_application != $this->id_application){
			throw new \Exception(
				"L'identifiant de l'application de l'archive {$archiveData->id_application} ne correspond pas à l'identifiant courant {$this->id_application}"
			);
		}
	}



    /**
     * @param $archive_filepath
     * @throws \Exception
     */
    private function validateArchiveSize($archive_filepath){
        $file_size = filesize($archive_filepath);
        if ( $file_size > self::ARCHIVE_MAX_SIZE_IN_BYTES){
            throw new \Exception(
                "La taille de l'archive ($file_size) dépasse la taille maximum autorisé (".self::ARCHIVE_MAX_SIZE_IN_BYTES.")"
            );
        }
    }

    //classif 7-1 nature 5 => si xml alors annexe obligatoire

    /**
     * @param ArchiveData $archiveData
     * @throws \Exception
     */
    private function validateActesBudgetaire(ArchiveData $archiveData){
        foreach($archiveData->fichierXML as $fichierXML){

            if (! $fichierXML instanceof MessageMetierActes) {
                continue;
            }

            /** @var MessageMetierActes $fichierXML */
            if ($fichierXML->code_nature_numerique != '5' ||
                $fichierXML->classification_1 != '7' ||
                    $fichierXML->classification_2 != '1'
            ) {
                continue;
            }

            //On recherche le fichier XML
            $file_totem = false;
            $file_principale = false;
            if (pathinfo($fichierXML->actes_filepath,PATHINFO_EXTENSION) == 'xml'){
                $file_totem = $fichierXML->actes_filepath;
                $file_principale = true;
            }
            if (! $file_totem){
                foreach($fichierXML->annexe_filepath as $filepath){
                    if (pathinfo($filepath,PATHINFO_EXTENSION) == 'xml'){
                        $file_totem = $filepath;
                    }
                }
            }
            if (!$file_totem){
                continue;
            }

            $xml = new PieceJointeXML($file_totem);

            if (!$xml->hasScellement()){
                throw new \Exception("Un document budgétaire XML doit comporter une balise de scellement");
            }

            if (empty($fichierXML->annexe_filepath) && !$xml->isCompteFinancierUnique()
            ){
                throw new \Exception("Un document budgétaire XML doit être accompagné d'au moins une annexe au format pdf, jpeg ou png");
            }

            if ($file_principale) {
                foreach ($fichierXML->annexe_filepath as $filepath) {
                    $extension = pathinfo($filepath, PATHINFO_EXTENSION);
                    if (!in_array($extension, array('pdf', 'jpg', 'png'))) {
                        throw new \Exception("Un document budgétaire XML doit être accompagné d'au moins une annexe au format pdf, jpeg ou png. $extension reçu");
                    }
                }
            }
        }
    }


	/**
	 * @param $archiveData
	 * @throws \Exception
	 */
    private function validateFichierFilename($archiveData,$authorized_nature,$authorized_typologie){
    	/** @var FichierXML $fichierXML */
		foreach($archiveData->fichierXML as $fichierXML){

		    /* Validation du numéro d'acte */
            $fname = basename($fichierXML->file_path);

            $messageMetierFilename = new MessageMetierFilename();

            $info_from_message = $messageMetierFilename->getInfoFromEnvelloppeName($fname);

            if($info_from_message['nature_acte'] != $fichierXML->code_nature_numerique) {
                throw new \Exception("La nature de l'actes du fichier métier ({$info_from_message['nature_acte']}) ".
                    "ne correspond pas au contenu XML ({$fichierXML->code_nature_numerique})");
            }
            if($info_from_message['numero_acte'] != $fichierXML->numero_interne){
                throw new \Exception(
                    "Le numéro interne du nom du fichier métier ({$info_from_message['numero_acte']}) " .
                    " ne correspond pas au contenu XML ({$fichierXML->numero_interne})");
            }

			if ($authorized_nature && $info_from_message['code_message'] == MessageMetierActes::CODE_MESSAGE &&! in_array($info_from_message['nature_acte'],$authorized_nature)){
				throw new \Exception("La nature {$info_from_message['nature_acte']} n'est pas autorisé pour le message métier $fname");
			}

			$file_name_list = [];
			foreach( $fichierXML->getFileList() as $file_list_id){


				if (! is_array($fichierXML->$file_list_id)){
					$file_name_list[] = $fichierXML->$file_list_id;
				} else {
					$file_name_list = array_merge($file_name_list,$fichierXML->$file_list_id);
				}
			}

			$message_metier_name = basename($fichierXML->file_path);

			if (! preg_match("#(^.*_)[0-9]+.xml$#",$message_metier_name,$matches)){
				throw new \Exception("Impossible de trouver le nom du message métier ".$message_metier_name);
			}

			$name_from_message_metier = $matches[1];


			foreach ($file_name_list as $file_name){

				$basename= basename($file_name);
				if ($this->is_mandatory_typologie && ! preg_match("#^.{5}-#",$basename)){
					throw new \Exception("Le fichier $basename n'a pas de typologie et celle-ci est obligatoire");
				}

				if (! preg_match("#^(.{5}-)?${name_from_message_metier}[0-9]+\..*#",$basename,$matches_typologie)){
					throw new \Exception("Le nom fichier $basename ne correspond pas au nom du message métier $message_metier_name");
				}

				if (empty($matches_typologie[1]) ){

					//Si la typologie est obligatoire, on sera sortie sur un throw précédent
					//Sinon, ben on tombe dans le cas pas obligatoire et si elle est vide, c'est bon.
					continue;
				}
				$typologie = $matches_typologie[1];

				if ($typologie) {
					$typologie = trim($typologie, "-");
				}
				if ($authorized_typologie && $info_from_message['code_message'] == MessageMetierActes::CODE_MESSAGE && !in_array($typologie, $authorized_typologie)) {
					throw new \Exception("La typologie $typologie n'est pas permise sur le fichier $basename");
				}

				if ($this->typologie_by_nature){
				    if (empty($this->typologie_by_nature[$fichierXML->code_nature_numerique][$typologie])){
				        throw new \Exception("La typologie $typologie n'est pas permise sur le fichier $basename pour la nature {$fichierXML->code_nature_numerique}");
                    }
                }

			}

		}

	}
}