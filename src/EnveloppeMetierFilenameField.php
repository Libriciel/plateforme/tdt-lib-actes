<?php

namespace Libriciel\LibActes;

class EnveloppeMetierFilenameField {
    public $is_ano;
	public $appli;
	public $info_appli;
	public $emetteur;
	public $destinataire;
	public $date;
	public $numero;
}