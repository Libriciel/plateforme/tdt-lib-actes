<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\FichierXML\FichierXML;

class ArchiveData {

    public $enveloppe_path;

    public $is_ano;

    /* Format Ymd */
    public $date_generation;

    public $numero_sequentiel;

    public $id_tdt;
    public $id_application;

    //Tel que défini en 7.2 de l'annexe 1
    public $service_controle;

    //Enveloppe MISILCL
    public $departement_pref;
    public $departement_spref;
    public $arrondissement_spref;
    public $departement_sgar;


    public $siren;
    public $departement;
    public $arrondissement;
    public $nature;

    public $email_referent;
    public $nom_referent;
    public $telephone_referent;

    public $adresses_retour = array();

    /** @var FichierXML[] */
    public $fichierXML = array();

    public $signatureEnveloppeMetier = array();
}