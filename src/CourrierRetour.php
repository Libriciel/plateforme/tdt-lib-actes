<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\FichierXML\MessageMetierActes;
use Libriciel\LibActes\FichierXML\MessageMetierCourrierSimple;
use Libriciel\LibActes\FichierXML\MessageMetierDefereTA;
use Libriciel\LibActes\FichierXML\MessageMetierDemandePieceComplementaire;
use Libriciel\LibActes\FichierXML\MessageMetierLettreObservations;
use Libriciel\LibActes\Utils\ObjectCopy;

class CourrierRetour {


    public function generateCourrierRetour(ArchiveData $archiveData,$messageMetierFilename,$code_message){

        $messageMetierActes = false;
        foreach($archiveData->fichierXML as $fichierXML){
            if (basename($fichierXML->file_path) == $messageMetierFilename){
                $messageMetierActes = $fichierXML;
                break;
            }
        }
        if (! $messageMetierActes){
            throw new \Exception("$messageMetierFilename n'a pas été trouvé dans l'archive");
        }
        if (! $messageMetierActes instanceof MessageMetierActes){
            throw new \Exception("$messageMetierFilename n'est pas un message 1-1");
        }

        if ($code_message == MessageMetierCourrierSimple::CODE_MESSAGE) {
            $messageMetierReponse = $this->getCourrierSimple($messageMetierActes);
        } elseif ($code_message == MessageMetierDemandePieceComplementaire::CODE_MESSAGE){
            $messageMetierReponse = $this->getDemandePC($messageMetierActes);
        } elseif ($code_message == MessageMetierLettreObservations::CODE_MESSAGE){
            $messageMetierReponse = $this->getLettreObservation($messageMetierActes);
        } elseif ($code_message == MessageMetierDefereTA::CODE_MESSAGE){
            $messageMetierReponse = $this->getDefereTA($messageMetierActes);
        } else {
            throw new \Exception("Not implemented");
        }

        $objectCopy = new ObjectCopy();
        $archiveReponse = new ArchiveData();
        $objectCopy->copy($archiveData,$archiveReponse);
        $archiveReponse->service_controle = "SPREF001";
        $archiveReponse->departement_pref = "001";
        $archiveReponse->fichierXML = array($messageMetierReponse);
        $archiveReponse->date_generation = date("Ymd");
        $archiveReponse->numero_sequentiel = mt_rand(1,999);


        return $archiveReponse;
    }

    public function getCourrierSimple(MessageMetierActes $messageMetierActes){
        $messageMetierReponse = new MessageMetierCourrierSimple();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($messageMetierActes,$messageMetierReponse);
        $messageMetierReponse->id_actes = $messageMetierActes->getUniqueID();
        $messageMetierReponse->date_courrier_pref = date("Y-m-d");
        $messageMetierReponse->document = __DIR__."/reponse/reponse.pdf";
        return $messageMetierReponse;
    }

    public function getDefereTA(MessageMetierActes $messageMetierActes){
        $messageMetierReponse = new MessageMetierDefereTA();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($messageMetierActes,$messageMetierReponse);
        $messageMetierReponse->id_actes = $messageMetierActes->getUniqueID();
        $messageMetierReponse->date_depot = date("Y-m-d");
        $messageMetierReponse->nature_illegalite = "NATURE_TEST";
        $messageMetierReponse->document = array(__DIR__."/reponse/reponse.pdf");
        return $messageMetierReponse;
    }

    public function getDemandePC(MessageMetierActes $messageMetierActes){
        $messageMetierReponse = new MessageMetierDemandePieceComplementaire();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($messageMetierActes,$messageMetierReponse);
        $messageMetierReponse->id_actes = $messageMetierActes->getUniqueID();
        $messageMetierReponse->date_courrier_pref = date("Y-m-d");
        $messageMetierReponse->document = __DIR__."/reponse/reponse.pdf";
        $messageMetierReponse->description_piece = "demande de pièces en plus";
        return $messageMetierReponse;
    }

    public function getLettreObservation(MessageMetierActes $messageMetierActes){
        $messageMetierReponse = new MessageMetierLettreObservations();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($messageMetierActes,$messageMetierReponse);
        $messageMetierReponse->id_actes = $messageMetierActes->getUniqueID();
        $messageMetierReponse->date_lettre_observation = date("Y-m-d");
        $messageMetierReponse->document = __DIR__."/reponse/reponse.pdf";
        $messageMetierReponse->motif = "motif";
        return $messageMetierReponse;
    }



}