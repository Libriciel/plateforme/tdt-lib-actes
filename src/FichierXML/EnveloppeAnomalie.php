<?php

namespace Libriciel\LibActes\FichierXML;

class EnveloppeAnomalie extends FichierXML {

    public function getCodeMessage() {
        return false;
    }

    public $date;
    public $nature_erreur;
    public $detail_erreur;

    public $siren;
    public $departement;
    public $arrondissement;
    public $nature;

    public $email_referent;
    public $nom_referent;
    public $telephone_referent;

    public $adresses_retour = array();
    public $fichier = array();
    public $signature = array();

    public function getMandatoryField(){
        return array('siren');
    }

    public function getPropertieMapping(){
        return array(
            "/actes:AnomalieEnveloppe/actes:Date" => 'date',
            "/actes:AnomalieEnveloppe/actes:Nature" => 'nature_erreur',
            "/actes:AnomalieEnveloppe/actes:Detail" => 'detail_erreur',

            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:Emetteur/actes:IDCL/@insee:SIREN' => 'siren',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:Emetteur/actes:IDCL/@actes:Departement' => 'departement',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:Emetteur/actes:IDCL/@actes:Arrondissement' => 'arrondissement',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:Emetteur/actes:IDCL/@actes:Nature' => 'nature',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:Emetteur/actes:Referent/actes:Nom' => 'nom_referent',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:Emetteur/actes:Referent/actes:Telephone' => 'telephone_referent',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:Emetteur/actes:Referent/actes:Email' => 'email_referent',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:AdressesRetour/actes:Email[]' => 'adresses_retour',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:FormulairesEnvoyes/actes:Formulaire[]/actes:NomFichier' => 'fichier',
            '/actes:AnomalieEnveloppe/actes:EnveloppeRecue/actes:FormulairesEnvoyes/actes:Formulaire[]/actes:Signature' => 'signature',
        );
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

}