<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierDefereTA extends FichierXML {

    const CODE_MESSAGE = "5-1";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_depot;
    public $nature_illegalite;

    public $document = array();
    public $signature = array();

    public function getPropertieMapping() {
        $result = array(
            "/actes:DefereTA/@actes:IDActe" => 'id_actes',
            "/actes:DefereTA/actes:DateDepot" => 'date_depot',
            "/actes:DefereTA/actes:NatureIllegalite" => 'nature_illegalite',
            "/actes:DefereTA/actes:PiecesJointes/actes:PieceJointe[]/actes:NomFichier" => 'document',
            "/actes:DefereTA/actes:PiecesJointes/actes:PieceJointe[]/actes:Signature" => 'signature',
        );

        return $result;
    }

    public function getFileList(){
        return array('document');
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }
}