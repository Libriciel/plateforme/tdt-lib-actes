<?php

namespace Libriciel\LibActes\FichierXML;

use Libriciel\LibActes\Utils\ObjectCopy;

class MessageMetierLettreObservations extends FichierXML {

    const CODE_MESSAGE = "4-1";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_lettre_observation;

    public $motif;

    public $document;
    public $signature;

    public function getMandatoryField(){
        return array('id_actes','date_lettre_observation');
    }

    public function getPropertieMapping() {
        $result = array(
            "/actes:LettreObservations/@actes:IDActe" => 'id_actes',
            "/actes:LettreObservations/@actes:DateCourrierPref" => 'date_lettre_observation',
            "/actes:LettreObservations/actes:Motif" => 'motif',
            "/actes:LettreObservations/actes:Document/actes:NomFichier" => 'document',
            "/actes:LettreObservations/actes:Document/actes:Signature" => 'signature',
        );

        return $result;
    }

    public function getFileList(){
        return array('document');
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

    public function getReponse($date_reception){
        $ar = new MessageMetierARLettreObservations();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($this,$ar);

        $ar->id_actes = $this->id_actes;
        $ar->date_courrier_pref = $this->date_lettre_observation;

        return $ar;
    }
}