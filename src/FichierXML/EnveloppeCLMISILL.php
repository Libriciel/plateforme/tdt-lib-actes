<?php

namespace Libriciel\LibActes\FichierXML;

use Libriciel\LibActes\Utils\ObjectCopy;

class EnveloppeCLMISILL extends FichierXML {

    public function getCodeMessage() {
        return false;
    }

	public $siren;
	public $departement;
	public $arrondissement;
	public $nature;

    public $email_referent;
    public $nom_referent;
    public $telephone_referent;

    public $adresses_retour = array();
	public $fichier = array();
	public $signature = array();

    public function getMandatoryField(){
        return array('siren','departement','nature');
    }

    public function getPropertieMapping(){
        return array(
            '/actes:EnveloppeCLMISILL/actes:Emetteur/actes:IDCL/@insee:SIREN' => 'siren',
            '/actes:EnveloppeCLMISILL/actes:Emetteur/actes:IDCL/@actes:Departement' => 'departement',
            '/actes:EnveloppeCLMISILL/actes:Emetteur/actes:IDCL/@actes:Arrondissement' => 'arrondissement',
            '/actes:EnveloppeCLMISILL/actes:Emetteur/actes:IDCL/@actes:Nature' => 'nature',
            '/actes:EnveloppeCLMISILL/actes:Emetteur/actes:Referent/actes:Nom' => 'nom_referent',
            '/actes:EnveloppeCLMISILL/actes:Emetteur/actes:Referent/actes:Telephone' => 'telephone_referent',
            '/actes:EnveloppeCLMISILL/actes:Emetteur/actes:Referent/actes:Email' => 'email_referent',
            '/actes:EnveloppeCLMISILL/actes:AdressesRetour/actes:Email[]' => 'adresses_retour',
            '/actes:EnveloppeCLMISILL/actes:FormulairesEnvoyes/actes:Formulaire[]/actes:NomFichier' => 'fichier',
            '/actes:EnveloppeCLMISILL/actes:FormulairesEnvoyes/actes:Formulaire[]/actes:Signature' => 'signature',
        );
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_CL_MI;
    }

    public function getReponse($date_reception) {
        $enveloppeMISIL = new EnveloppeMISILLCL();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($this,$enveloppeMISIL);
        $enveloppeMISIL->departement_spref = "012";
        $enveloppeMISIL->arrondissement_spref = "1";

        return $enveloppeMISIL;
    }

}