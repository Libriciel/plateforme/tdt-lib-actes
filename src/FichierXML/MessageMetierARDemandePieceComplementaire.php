<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierARDemandePieceComplementaire extends FichierXML {

    const CODE_MESSAGE = "3-2";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;

    public function getMandatoryField(){
        return array('id_actes','date_courrier_pref');
    }

    public function getPropertieMapping() {
        $result = array(
            "/actes:ARDemandePieceComplementaire/@actes:IDActe" => 'id_actes',
            "/actes:ARDemandePieceComplementaire/@actes:DateCourrierPref" => 'date_courrier_pref',
        );

        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_CL_MI;
    }

}