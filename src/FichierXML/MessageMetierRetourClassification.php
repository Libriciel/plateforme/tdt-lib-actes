<?php

namespace Libriciel\LibActes\FichierXML;

/**
 * Class MessageMetierRetourClassification
 * @package Libriciel\LibActes\FichierXML
 *
 * Attention, implémentation très très partiel !!
 *
 */
class MessageMetierRetourClassification extends FichierXML {

    const CODE_MESSAGE = "7-2";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $date_classification;

    public $nature_actes_libelle = array();
    public $nature_actes_type_abrege = array();
    public $nature_actes_code = array();

    public $matiere1_code = array();
    public $matiere1_libelle = array();

    public $matiere2_code = array();
    public $matiere2_libelle = array();

    public function getPropertieMapping() {
        $result = array(
            "/actes:RetourClassification/actes:DateClassification" => 'date_classification',
            "/actes:RetourClassification/actes:NaturesActes/actes:NatureActe[]/@actes:CodeNatureActe" => 'nature_actes_code',
            "/actes:RetourClassification/actes:NaturesActes/actes:NatureActe[]/@actes:Libelle" => 'nature_actes_libelle',
            "/actes:RetourClassification/actes:NaturesActes/actes:NatureActe[]/@actes:TypeAbrege" => 'nature_actes_type_abrege',
            "/actes:RetourClassification/actes:Matieres/actes:Matiere1[]/@actes:CodeMatiere" => "matiere1_code",
            "/actes:RetourClassification/actes:Matieres/actes:Matiere1[]/@actes:Libelle" => "matiere1_libelle",
            "/actes:RetourClassification/actes:Matieres/actes:Matiere1/actes:Matiere2[]/@actes:CodeMatiere" => "matiere2_code",
            "/actes:RetourClassification/actes:Matieres/actes:Matiere1/actes:Matiere2[]/@actes:Libelle" => "matiere2_libelle",

        );

        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

}