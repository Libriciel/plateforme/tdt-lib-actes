<?php

namespace Libriciel\LibActes\FichierXML;

use Libriciel\LibActes\Utils\ObjectCopy;

class MessageMetierPieceComplementaire extends FichierXML {

    const CODE_MESSAGE = "3-4";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;

    public $document = array();
    public $signature = array();


    public function getMandatoryField(){
        return array('id_actes','date_courrier_pref');
    }

    public function getPropertieMapping() {
        $result = array(
            "/actes:PieceComplementaire/@actes:IDActe" => 'id_actes',
            "/actes:PieceComplementaire/@actes:DateCourrierPref" => 'date_courrier_pref',
            "/actes:PieceComplementaire/actes:Documents/actes:Document[]/actes:NomFichier" => 'document',
            "/actes:PieceComplementaire/actes:Documents/actes:Document[]/actes:Signature" => 'signature',
        );

        return $result;
    }

    public function getFileList(){
        return array('document');
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

    public function getReponse($date_reception){
        $ar = new MessageMetierARPieceComplementaire();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($this,$ar);

        $ar->id_actes = $this->id_actes;
        $ar->date_courrier_pref = $this->date_courrier_pref;
        $ar->date_reception = $date_reception;

        return $ar;
    }


}