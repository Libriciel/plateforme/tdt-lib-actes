<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierARAnnulation extends FichierXML {

    const CODE_MESSAGE = "6-2";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_reception;

    public function getPropertieMapping() {
        $result = array(
            "/actes:ARAnnulation/@actes:IDActe" => 'id_actes',
            "/actes:ARAnnulation/@actes:DateReception" => 'date_reception',
        );
        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

}