<?php

namespace Libriciel\LibActes\FichierXML;

abstract class FichierXML {

    const SENS_CL_MI = "sens-cl-mi";
    const SENS_MI_CL = "sens-mi-cl";

    public $departement;
    public $siren;

    public $date_actes;
    public $numero_interne;
    public $code_nature_numerique;

    public $file_path;


    abstract public function getCodeMessage();
    abstract public function getPropertieMapping();
    abstract public function getFileList();
    abstract public function getSpecialValue();
    abstract public function getSens();
    public function getMandatoryField(){ return array();}

    /**
     * @param bool $date_reception
     * @return bool | FichierXML
     */
    public function getReponse($date_reception){return false;}

    public function validate(){return false;}


}