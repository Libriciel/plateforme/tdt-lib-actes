<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierCourrierSimple extends FichierXML {

    const CODE_MESSAGE = "2-1";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;

    public $document;
    public $signature;

    public function getPropertieMapping() {
        $result = array(
            "/actes:CourrierSimple/@actes:IDActe" => 'id_actes',
            "/actes:CourrierSimple/@actes:DateCourrierPref" => 'date_courrier_pref',
            "/actes:CourrierSimple/actes:Document/actes:NomFichier" => 'document',
            "/actes:CourrierSimple/actes:Document/actes:Signature" => 'signature',
        );

        return $result;
    }

    public function getFileList(){
        return array('document');
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

}