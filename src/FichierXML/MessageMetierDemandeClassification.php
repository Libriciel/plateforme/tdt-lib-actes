<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierDemandeClassification extends FichierXML {

    const CODE_MESSAGE = "7-1";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    //Obligatoire pour tdt-lib-actes !
    public $date_classification;

    public function getPropertieMapping() {
        $result = array(
            "/actes:DemandeClassification/actes:DateClassification" => 'date_classification',
        );

        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_CL_MI;
    }

}