<?php

namespace Libriciel\LibActes\FichierXML;

class EnveloppeMISILLCL extends FichierXML {

    public function getCodeMessage() {
        return false;
    }

	public $siren;
    public $departement;
    public $arrondissement;

	public $departement_pref;

    public $departement_spref;
    public $arrondissement_spref;

    public $departement_sgar;

	public $nature;
	public $type_emetteur;

    public $email_referent;
    public $nom_referent;
    public $telephone_referent;

    public $fichier = array();
	public $signature = array();

    public function getMandatoryField(){
        return array('siren');
    }

    public function getPropertieMapping(){
        return array(
            "/actes:EnveloppeMISILLCL/actes:Emetteur/actes:IDSGAR/@actes:Departement" => 'departement_sgar',
            "/actes:EnveloppeMISILLCL/actes:Emetteur/actes:IDPref/@actes:Departement" => 'departement_pref',
            "/actes:EnveloppeMISILLCL/actes:Emetteur/actes:IDSousPref/@actes:Departement" => 'departement_spref',
            "/actes:EnveloppeMISILLCL/actes:Emetteur/actes:IDSousPref/@actes:Arrondissement" => 'arrondissement_spref',
            '/actes:EnveloppeMISILLCL/actes:Emetteur/actes:Referent/actes:Nom' => 'nom_referent',
            '/actes:EnveloppeMISILLCL/actes:Emetteur/actes:Referent/actes:Telephone' => 'telephone_referent',
            '/actes:EnveloppeMISILLCL/actes:Emetteur/actes:Referent/actes:Email' => 'email_referent',
            '/actes:EnveloppeMISILLCL/actes:FormulairesEnvoyes/actes:Formulaire[]/actes:NomFichier' => 'fichier',
            '/actes:EnveloppeMISILLCL/actes:FormulairesEnvoyes/actes:Formulaire[]/actes:Signature' => 'signature',
            '/actes:EnveloppeMISILLCL/actes:Destinataire/@insee:SIREN' => 'siren'
        );
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }


}