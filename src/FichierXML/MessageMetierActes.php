<?php

namespace Libriciel\LibActes\FichierXML;

use Libriciel\LibActes\Utils\ObjectCopy;


class MessageMetierActes extends FichierXML {

    const CODE_MESSAGE = "1-1";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $departement;
    public $siren;

    public $objet;
    public $date_actes;
    public $numero_interne;
    public $code_nature_numerique;

    public $classification_date_version;
    public $classification_1;
    public $classification_2;
    public $classification_3;
    public $classification_4;
    public $classification_5;

    public $actes_filepath;
    public $actes_signature;

    public $annexe_filepath = array();
    public $annexe_signature = array();

    public $precedent_identifiant_acte;

    public $document_papier;

    public $nombre_annexes;

    public function getPropertieMapping(){
        $result = array(
            "/actes:Acte/@actes:Date" => 'date_actes',
            "/actes:Acte/@actes:NumeroInterne" => 'numero_interne',
            "/actes:Acte/@actes:CodeNatureActe" => 'code_nature_numerique',
            "/actes:Acte/actes:Objet" => 'objet',
            "/actes:Acte/actes:PrecedentIdentifiantActe" => 'precedent_identifiant_acte',
            "/actes:Acte/actes:ClassificationDateVersion" => 'classification_date_version',
            "/actes:Acte/actes:Document/actes:NomFichier" => 'actes_filepath',
            "/actes:Acte/actes:Document/actes:Signature" => 'actes_signature',
            "/actes:Acte/actes:Annexes/@actes:Nombre" => 'nombre_annexes',
            "/actes:Acte/actes:Annexes/actes:Annexe[]/actes:NomFichier" => 'annexe_filepath',
            "/actes:Acte/actes:Annexes/actes:Annexe[]/actes:Signature" => 'annexe_signature',
            "/actes:Acte/actes:DocumentPapier" => 'document_papier',
        );

        for($i=1; $i<=5; $i++){
            $result["/actes:Acte/actes:CodeMatiere$i/@actes:CodeMatiere"] = "classification_$i";
        }

        return $result;
    }

    public function getFileList(){
        return array( 'actes_filepath', 'annexe_filepath');
    }

    public function getSpecialValue(){
        return array(
            'nombre_annexes' => count($this->annexe_filepath)
        );
    }

    public function getSens(){
        return FichierXML::SENS_CL_MI;
    }

    public function getMandatoryField(){
        return array('objet','numero_interne');
    }

    public function getReponse($date_reception){
        $arActes = new MessageMetierARActes();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($this,$arActes);

        foreach($this->getFileList() as $filekey){
            if (is_array($this->$filekey)){
                foreach($this->$filekey as $i => $filepath){
                    $ar_filekey = $arActes->$filekey;
                    $ar_filekey[$i] = basename($filepath);
                }
            } else {
                $arActes->$filekey =  basename($this->$filekey);
            }
        }

        $arActes->id_actes = $this->getUniqueID();
        $arActes->classification_date_version_en_cours = $arActes->classification_date_version;
        $arActes->date_reception = $date_reception;
        return $arActes;
    }


    public function getUniqueID(){
        $codification_nature =
            array(1=>"DE","AR","AI","CC","BF","AU");
        //034-000000000-20170701-20170728C-AI
        return sprintf(
            "%s-%s-%s-%s-%s",
            $this->departement,
            $this->siren,
            date("Ymd", strtotime($this->date_actes)),
            $this->numero_interne,
            $codification_nature[$this->code_nature_numerique]
        );
    }

    public function validate(){
    	$count = count($this->annexe_filepath);

    	if ($this->nombre_annexes != $count){
			throw new \Exception("Le nombre d'annexe trouvé ($count) ne correspond pas à celui indiqué dans le fichier XML ({$this->nombre_annexes})");
		}

	}

}