<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierReponseCourrierSimple extends FichierXML {

    const CODE_MESSAGE = "2-2";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;

    public $document;
    public $signature;

    public function getMandatoryField(){
        return array('id_actes','date_courrier_pref');
    }

    public function getPropertieMapping() {
        $result = array(
            "/actes:ReponseCourrierSimple/@actes:IDActe" => 'id_actes',
            "/actes:ReponseCourrierSimple/@actes:DateCourrierPref" => 'date_courrier_pref',
            "/actes:ReponseCourrierSimple/actes:Document/actes:NomFichier" => 'document',
            "/actes:ReponseCourrierSimple/actes:Document/actes:Signature" => 'signature',
        );

        return $result;
    }

    public function getFileList(){
        return array('document');
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_CL_MI;
    }

}