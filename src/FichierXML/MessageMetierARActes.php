<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierARActes extends FichierXML {

    const CODE_MESSAGE = "1-2";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_reception;
    public $classification_date_version_en_cours;

    public $objet;
    public $date_actes;
    public $numero_interne;
    public $code_nature_numerique;

    public $classification_date_version;
    public $classification_1;
    public $classification_2;
    public $classification_3;
    public $classification_4;
    public $classification_5;

    public $actes_filepath;
    public $actes_signature;

    public $nombre_annexes;
    public $annexe_filepath = array();
    public $annexe_signature = array();

    public $precedent_identifiant_acte;
    public $document_papier;

    public function getPropertieMapping() {
        $result = array(
            "/actes:ARActe/@actes:IDActe" => 'id_actes',
            "/actes:ARActe/@actes:DateReception" => 'date_reception',
            "/actes:ARActe/actes:ClassificationDateVersionEnCours" => 'classification_date_version_en_cours',

            "/actes:ARActe/actes:ActeRecu/@actes:Date" => 'date_actes',
            "/actes:ARActe/actes:ActeRecu/@actes:NumeroInterne" => 'numero_interne',
            "/actes:ARActe/actes:ActeRecu/@actes:CodeNatureActe" => 'code_nature_numerique',
            "/actes:ARActe/actes:ActeRecu/actes:Objet" => 'objet',
            "/actes:ARActe/actes:ActeRecu/actes:PrecedentIdentifiantActe" => 'precedent_identifiant_acte',
            "/actes:ARActe/actes:ActeRecu/actes:ClassificationDateVersion" => 'classification_date_version',
            "/actes:ARActe/actes:ActeRecu/actes:Document/actes:NomFichier" => 'actes_filepath',
            "/actes:ARActe/actes:ActeRecu/actes:Document/actes:Signature" => 'actes_signature',
            "/actes:ARActe/actes:ActeRecu/actes:Annexes/@actes:Nombre" => 'nombre_annexes',
            "/actes:ARActe/actes:ActeRecu/actes:Annexes/actes:Annexe[]/actes:NomFichier" => 'annexe_filepath',
            "/actes:ARActe/actes:ActeRecu/actes:Annexes/actes:Annexe[]/actes:Signature" => 'annexe_signature',
            "/actes:ARActe/actes:ActeRecu/actes:DocumentPapier" => 'document_papier',

        );

        for($i=1; $i<=5; $i++){
            $result["/actes:ARActe/actes:ActeRecu/actes:CodeMatiere$i/@actes:CodeMatiere"] = "classification_$i";
        }
        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

}