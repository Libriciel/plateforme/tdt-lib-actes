<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierARPieceComplementaire extends FichierXML {

    const CODE_MESSAGE = "3-5";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;
    public $date_reception;

    public function getPropertieMapping() {
        $result = array(
            "/actes:ARPieceComplementaire/actes:InfosCourrierPref/@actes:IDActe" => 'id_actes',
            "/actes:ARPieceComplementaire/actes:InfosCourrierPref/@actes:DateCourrierPref" => 'date_courrier_pref',
            "/actes:ARPieceComplementaire/@actes:DateReception" => 'date_reception',
        );

        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }
}