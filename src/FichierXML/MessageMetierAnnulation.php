<?php

namespace Libriciel\LibActes\FichierXML;

use Libriciel\LibActes\Utils\ObjectCopy;

class MessageMetierAnnulation extends FichierXML {

    const CODE_MESSAGE = "6-1";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;

    public function getMandatoryField(){
        return array('id_actes');
    }

    public function getPropertieMapping() {
        $result = array(
            "/actes:Annulation/@actes:IDActe" => 'id_actes',
        );
        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_CL_MI;
    }

    public function getReponse($date_reception){
        $ar = new MessageMetierARAnnulation();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($this,$ar);

        $ar->id_actes = $this->id_actes;
        $ar->date_reception = $date_reception;

        return $ar;
    }

}