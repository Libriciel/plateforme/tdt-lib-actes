<?php

namespace Libriciel\LibActes\FichierXML;

use Libriciel\LibActes\Utils\ObjectCopy;

class MessageMetierRejetLettreObservations extends FichierXML {

    const CODE_MESSAGE = "4-3";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;

    public $document;
    public $signature;

    public function getMandatoryField(){
        return array('id_actes','date_courrier_pref');
    }

    public function getPropertieMapping() {
        $result = array(
            "/actes:RejetLettreObservations/@actes:IDActe" => 'id_actes',
            "/actes:RejetLettreObservations/@actes:DateCourrierPref" => 'date_courrier_pref',
            "/actes:RejetLettreObservations/actes:Document/actes:NomFichier" => 'document',
            "/actes:RejetLettreObservations/actes:Document/actes:Signature" => 'signature',
        );

        return $result;
    }

    public function getFileList(){
        return array('document');
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_CL_MI;
    }

    public function getReponse($date_reception){
        $ar = new MessageMetierARReponseRejetLettreObservations();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($this,$ar);

        $ar->id_actes = $this->id_actes;
        $ar->date_courrier_pref = $this->date_courrier_pref;
        $ar->date_reception = $date_reception;

        return $ar;
    }

}