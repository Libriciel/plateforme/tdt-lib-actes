<?php

namespace Libriciel\LibActes\FichierXML;

use Libriciel\LibActes\Utils\ObjectCopy;


class MessageMetierDemandePieceComplementaire extends FichierXML {

    const CODE_MESSAGE = "3-1";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;

    public $description_piece;

    public $document;
    public $signature;

    public function getPropertieMapping() {
        $result = array(
            "/actes:DemandePieceComplementaire/@actes:IDActe" => 'id_actes',
            "/actes:DemandePieceComplementaire/@actes:DateCourrierPref" => 'date_courrier_pref',
            "/actes:DemandePieceComplementaire/actes:DescriptionPieces" => 'description_piece',
            "/actes:DemandePieceComplementaire/actes:Document/actes:NomFichier" => 'document',
            "/actes:DemandePieceComplementaire/actes:Document/actes:Signature" => 'signature',
        );

        return $result;
    }

    public function getFileList(){
        return array('document');
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }

    public function getReponse($date_reception){
        $ar = new MessageMetierARDemandePieceComplementaire();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($this,$ar);

        $ar->id_actes = $this->id_actes;
        $ar->date_courrier_pref = $this->date_courrier_pref;

        return $ar;
    }
}