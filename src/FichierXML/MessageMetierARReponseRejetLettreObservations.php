<?php

namespace Libriciel\LibActes\FichierXML;

class MessageMetierARReponseRejetLettreObservations extends FichierXML {

    const CODE_MESSAGE = "4-5";

    public function getCodeMessage() {
        return self::CODE_MESSAGE;
    }

    public $id_actes;
    public $date_courrier_pref;
    public $date_reception;

    public function getPropertieMapping() {
        $result = array(
            "/actes:ARReponseRejetLettreObservations/actes:InfosCourrierPref/@actes:IDActe" => 'id_actes',
            "/actes:ARReponseRejetLettreObservations/actes:InfosCourrierPref/@actes:DateCourrierPref" => 'date_courrier_pref',
            "/actes:ARReponseRejetLettreObservations/@actes:DateReception" => 'date_reception',
        );

        return $result;
    }

    public function getFileList(){
        return array();
    }

    public function getSpecialValue(){
        return array();
    }

    public function getSens(){
        return FichierXML::SENS_MI_CL;
    }
}