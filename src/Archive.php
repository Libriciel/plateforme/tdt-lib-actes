<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\FichierXML\EnveloppeMISILLCL;
use Libriciel\LibActes\FichierXML\FichierXML;
use Libriciel\LibActes\FichierXML\MessageMetierDemandeClassification;
use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\FichierXML\EnveloppeCLMISILL;

class Archive {

    public function generateFiles(
        ArchiveData $archiveData,
        $output_directory
    ){
        $message_metier = array();

        $transmissionGenerator = new TransmissionGenerator();

        foreach($archiveData->fichierXML as $transmissionActesData){
            $message_metier[] = basename($transmissionGenerator->generateFiles(
                $transmissionActesData,
                $output_directory
            ));
        }

        $sens_transmission = $this->getSensTransmission($archiveData);

        if ($sens_transmission == FichierXML::SENS_CL_MI) {
            $enveloppeMetierData = new EnveloppeCLMISILL();
        } else {
            $enveloppeMetierData = new EnveloppeMISILLCL();
        }
        foreach (get_object_vars($enveloppeMetierData) as $key => $value) {
            if (isset($archiveData->$key)) {
                $enveloppeMetierData->$key = $archiveData->$key;
            }
        }
        $enveloppeMetierData->fichier = $message_metier;

        $actesXML = new ActesXML();

        $enveloppe_metier_content = $actesXML->getXML($enveloppeMetierData);

        $emetteur = $sens_transmission==FichierXML::SENS_CL_MI?$archiveData->siren:$archiveData->service_controle;
        $destinataire = $sens_transmission==FichierXML::SENS_CL_MI?"":$archiveData->siren;

        $date = date("Ymd",strtotime($archiveData->date_generation));
        $numero = $archiveData->numero_sequentiel;

        $appli = $archiveData->id_application;

        $enveloppe_name = "$appli--$emetteur-$destinataire-$date-$numero.xml";

        file_put_contents($output_directory."/$enveloppe_name",$enveloppe_metier_content);

        return $output_directory."/$enveloppe_name";
    }

    private function getSensTransmission(ArchiveData $archiveData) {
        if (empty($archiveData->fichierXML)){
            throw new \Exception(
                "Aucune transmission trouvée : impossible de déterminer le sens de la transmission"
            );
        }
        $sens = $archiveData->fichierXML[0]->getSens();
        foreach($archiveData->fichierXML as $fichierXML){
            if ($fichierXML->getSens() != $sens){
                throw new \Exception(
                    "Il y a des transmissions dans les deux sens : impossible de générer l'enveloppe métier"
                );
            }
        }
        return $sens;
    }

    public function generateZip(
        ArchiveData $archiveData,
        $output_directory
    ){
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $enveloppe_metier_name = $this->generateFiles($archiveData,$tmp_dir);

        $archiveFilename = new ArchiveFilename();
        $targz_filename = $archiveFilename->getFilename($archiveData->id_tdt,$enveloppe_metier_name);

        $tar_filename = substr($targz_filename,0,-3);

        $pharData = new \PharData($output_directory."/".$tar_filename);

        foreach(glob("$tmp_dir/*") as $file) {
            $pharData->addFile($file, basename($file));
        }

        $pharData->compress(\Phar::GZ);
        unlink($output_directory."/".$tar_filename);

        $tmpDir->destruct();
        return $output_directory."/".$tar_filename.".gz";
    }

	/**
	 * @param $enveloppe_metier_filepath
	 * @return ArchiveData
	 * @throws \Exception
	 */
    public function getArchiveData($enveloppe_metier_filepath){

        if (! file_exists($enveloppe_metier_filepath)){
            throw new \Exception(
                "L'enveloppe métier " .basename($enveloppe_metier_filepath)." n'a pas été trouvé dans l'archive"
            );
        }

        $xml = file_get_contents($enveloppe_metier_filepath);

        $archiveData = new ArchiveData();

        $actesXML = new ActesXML();
        $enveloppeData = $actesXML->getDataFromXML($xml);

        foreach(get_object_vars($enveloppeData) as $member => $value){
            if (property_exists($archiveData,$member)) {
                $archiveData->$member = $value;
            }
        }

        $actesEnveloppeField = $this->getInfoFromEnveloppeFilename(basename($enveloppe_metier_filepath));
        $archiveData->enveloppe_path = $enveloppe_metier_filepath;
        $archiveData->is_ano = $actesEnveloppeField->is_ano;
        $archiveData->id_application = $actesEnveloppeField->appli;
        $archiveData->numero_sequentiel = $actesEnveloppeField->numero;
        $archiveData->date_generation = $actesEnveloppeField->date;

        if ($actesEnveloppeField->destinataire) {
            $archiveData->service_controle = $actesEnveloppeField->emetteur;

            if (! $actesEnveloppeField->emetteur){
            	throw new \Exception("L'enveloppe ne contient pas d'emetteur");
			}

        } else {
            if ($archiveData->siren != $actesEnveloppeField->emetteur){
                throw new \Exception(
                    "Le numéro SIREN du nom du fichier {$actesEnveloppeField->emetteur} diffère de celui trouvé dans l'enveloppe métier {$archiveData->siren}"
                );
            }
        }

        $actesXMLHelper = new ActesXSD();

        $simpleXML = $actesXMLHelper->getSimpleXMLForActesContent($xml);
        $message_metier_file_list = array();
        if (! $archiveData->is_ano) {
            $message_metier_file_list = $simpleXML->xpath(
                "//actes:FormulairesEnvoyes/actes:Formulaire/actes:NomFichier"
            );
        }

        $files_ok = array(".","..",basename($enveloppe_metier_filepath));

        foreach($message_metier_file_list as $message_metier_file){
            $file = dirname($enveloppe_metier_filepath)."/".strval($message_metier_file);
            $files_ok[] = strval($message_metier_file);
            $transmissionActes = new TransmissionGenerator();
            $fichierXML = $transmissionActes->getTransmissionData($file,$enveloppeData instanceof EnveloppeCLMISILL);
            $archiveData->fichierXML[] = $fichierXML;
            foreach($fichierXML->getFileList() as $file_attribute){

                if (is_array($fichierXML->$file_attribute)){
                    foreach($fichierXML->$file_attribute as $fichier){
                        $files_ok[] = basename($fichier);
                    }
                } else {
                    $files_ok[] = basename($fichierXML->$file_attribute);
                }
            }
            if ($archiveData->siren != $fichierXML->siren){
                //Ca n'est pas explicitement réclamé par le cahier des charges !
                throw new \Exception(
                    "Le numéro SIREN du message métier {$fichierXML->siren} diffère de celui trouvé dans l'enveloppe métier {$archiveData->siren}"
                );
            }
            if ($archiveData->departement && $archiveData->departement != $fichierXML->departement){
                //Ca n'est pas explicitement réclamé par le cahier des charges !
                throw new \Exception(
                    "Le numéro du département du message métier {$fichierXML->departement} diffère de celui trouvé dans l'enveloppe métier {$archiveData->departement}"
                );
            }
        }
        foreach($files_ok as $files_to_test){
            if (in_array($files_to_test,array('.','..'))){
                continue;
            }
            if ( ! file_exists(dirname($enveloppe_metier_filepath)."/".$files_to_test)){
                throw new \Exception("Le fichier $files_to_test est référencé, mais il n'est pas présent dans l'archive");
            }
        }

        $files_present = scandir(dirname($enveloppe_metier_filepath));

        $files = array_diff($files_present,$files_ok);
        if ($enveloppeData instanceof EnveloppeCLMISILL && $files){
            throw new \Exception("L'archive présente au moins un fichier en trop : ".implode(",",$files));
        }


        return $archiveData;
    }

    public function getArchiveDataFromFolder($folder){
        $enveloppeMetierFilename = new EnveloppeMetierFilename();
        if(!is_dir($folder)){
            throw new \Exception("Le répertoire $folder n'existe pas");
        }
        /** @var array $files */
        $files = scandir($folder) ?: [];
        $files = array_diff($files,array('.','..'));
        $enveloppe_metier_filename = false;
        foreach($files as $file){
            if ($enveloppeMetierFilename->isEnveloppeFilename($file)){
                $enveloppe_metier_filename = $file;
                break;
            }
        }
        if (! $enveloppe_metier_filename){
            throw new \Exception("Aucun fichier de type enveloppe métier n'a été trouvé dans le répertoire $folder");
        }
        return $this->getArchiveData($folder."/".$enveloppe_metier_filename);
    }

	/**
	 * @param $tarball_path
	 * @param $output_dir
	 * @return ArchiveData
	 * @throws \Exception
	 */
    public function getArchiveDataFromTarball($tarball_path, $output_dir){
        $command = "tar xzf $tarball_path --directory $output_dir 2>&1";
		exec($command, $output, $return_var);
		if ($return_var != 0) {
			throw new \Exception("Erreur ($return_var) lors de la décompression de l'archive $tarball_path : " . implode("\n", $output));
		}
        $enveloppe_filename = substr(basename($tarball_path),0, -7).".xml";
        $enveloppe_filename = substr($enveloppe_filename,4);

        $enveloppe_metier_path =
            $output_dir ."/$enveloppe_filename";
        $archiveData =  $this->getArchiveData($enveloppe_metier_path);
        $archiveData->id_tdt = substr(basename($tarball_path),0,3);
        return $archiveData;
    }

    private function getInfoFromEnveloppeFilename($enveloppe_filename) {
        $actesEnveloppeFilename = new EnveloppeMetierFilename();
        return $actesEnveloppeFilename->getEnveloppeField($enveloppe_filename);
    }

    public function generateReponseFromTarball($tarball_path,$output_directory) {
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $this->getArchiveDataFromTarball($tarball_path, $tmp_dir);
        $this->generateReponse($tmp_dir,$output_directory);
        $tmpDir->destruct();
    }

    public function generateReponse($input_directory,$output_directory){
        $transmissionGenerator = new TransmissionGenerator();
        $archiveData = $this->getArchiveDataFromFolder($input_directory);

        $message_metier = array();
        foreach ($archiveData->fichierXML as $fichierXML){

            if ($fichierXML instanceof MessageMetierDemandeClassification){
                $nom_message_classification = $fichierXML->departement."-".$fichierXML->siren."----7-2_0.xml";
                copy(__DIR__."/classification/classification.xml",$output_directory."/".$nom_message_classification);
                $message_metier[] = $nom_message_classification;

            } else {

                $reponse = $fichierXML->getReponse(date("Y-m-d"));
                if ($reponse) {
                    $message_metier[] = basename($transmissionGenerator->generateFiles(
                        $reponse,
                        $output_directory
                    ));
                }
            }
        }
        if (! $message_metier){
            return false;
        }


        $actesXML = new ActesXML();
        /** @var EnveloppeCLMISILL $envelope */
        $envelope = $actesXML->getDataFromXML(file_get_contents($archiveData->enveloppe_path));

        $envelopeXML = $envelope->getReponse(false);
        $envelopeXML->fichier = $message_metier;

        $xml1 = $actesXML->getXML($envelopeXML);

        $emetteur = "SPREF001";
        $destinataire = $archiveData->siren;
        $date = date("Ymd");
        $numero = $archiveData->numero_sequentiel;
        $appli = $archiveData->id_application;

        $enveloppe_name = "$appli--$emetteur-$destinataire-$date-$numero.xml";

        file_put_contents($output_directory."/".$enveloppe_name,$xml1);
        return true;
    }



}