<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\FichierXML\FichierXML;


class MessageMetierFilename {


    public function getInfoFromEnvelloppeName($enveloppe_filename){
        // DPT-SIREN-DATEACTE-NUMACTE-NATUREACTE-CODEMSG_NUMERO
        if (! preg_match(
            '#^(\d{3}|02A|02B)-(\d{9})-(\d{8})?-([^-]{1,15})?-([^-]{2})?-(.{3})_(\d{1,5}).xml$#',
            $enveloppe_filename,
            $matches
        )) {
            throw new \Exception ("Le nom du fichier enveloppe $enveloppe_filename est mal formé.");
        }
        array_shift($matches);

        $result = array();
        $info = array('departement','siren','date_acte','numero_acte','nature_acte','code_message','numero');
        foreach($info as $i => $key){
            $result[$key] = $matches[$i];
        }

        if ($result['date_acte']) {
            preg_match("#(\d{4})(\d{2})(\d{2})#",$result['date_acte'],$matches_date);

            array_shift($matches_date);
            list($year, $month, $day) = $matches_date;
            $result['date_acte'] = "$year-$month-$day";
        }
        if ($result['nature_acte']) {
            $result['nature_acte'] = array_flip($this->codification_nature)[$result['nature_acte']];
        }
        return $result;
    }

    public function getNomFichier(FichierXML $fichierXML,
                                   $original_filepath,
                                   $num_fichier
    ){
        $extension = pathinfo($original_filepath,PATHINFO_EXTENSION);
        if ($fichierXML->date_actes) {
            $date = date("Ymd", strtotime($fichierXML->date_actes));
        } else {
            $date ="";
        }
        $code_nature_abrege = $this->getCodeNatureAbregee($fichierXML);
        return "{$fichierXML->departement}-".
            "{$fichierXML->siren}-" .
            "{$date}-".
            "{$fichierXML->numero_interne}-" .
            "$code_nature_abrege-".
            $fichierXML->getCodeMessage() . "_".
            "$num_fichier.$extension";
    }

    public function getNomMessageMetier(FichierXML $fichierXML){
        return $this->getNomFichier($fichierXML,"fichier.xml",0);
    }

    private $codification_nature =
        array(1=>"DE","AR","AI","CC","BF","AU");


    private function getCodeNatureAbregee(FichierXML $transmissionData){
        if ($transmissionData->code_nature_numerique) {
            return $this->codification_nature[$transmissionData->code_nature_numerique];
        } else {
            return null;
        }
    }
}