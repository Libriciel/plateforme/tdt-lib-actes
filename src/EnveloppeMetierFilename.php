<?php

namespace Libriciel\LibActes;

class EnveloppeMetierFilename {

    const ENVELOPE_FILENAME_REGEXP = "#^(ANO_)?(.{4})-([^-]*)-([^-]*)-([^-]*)-([^-]{8})-([0-9]{1,5})\.xml$#";

	public function validate($enveloppe_filename){
		return (bool) $this->getEnveloppeField($enveloppe_filename);
	}

	public function isEnveloppeFilename($enveloppe_filename){
        return (bool) preg_match(self::ENVELOPE_FILENAME_REGEXP,$enveloppe_filename);
    }

	public function getEnveloppeField($enveloppe_filename){
		if (! preg_match(self::ENVELOPE_FILENAME_REGEXP,$enveloppe_filename,$matches)){
			throw new \Exception ("Le nom du fichier enveloppe $enveloppe_filename est mal formé.");
		}
		array_shift($matches);
		list($is_ano,$appli,$infos_appli,$emetteur,$destinataire,$date,$numero) = $matches;

		$actesEnveloppeField = new EnveloppeMetierFilenameField();
        $actesEnveloppeField->is_ano = $is_ano?true:false;
		$actesEnveloppeField->appli = $appli;
		$actesEnveloppeField->info_appli = $infos_appli;
		$actesEnveloppeField->emetteur = $emetteur;
		$actesEnveloppeField->destinataire = $destinataire;
		$actesEnveloppeField->numero = $numero;
        $actesEnveloppeField->date = $date;

		preg_match("#(\d{4})(\d{2})(\d{2})#",$date,$matches_date);

		array_shift($matches_date);
		list($year,$month,$day) = $matches_date;

		if (! checkdate($month, $day, $year)){
			throw new \Exception(
				"La date $date trouvé dans le nom du fichier enveloppe $enveloppe_filename n'existe pas"
			);
		}
		return $actesEnveloppeField;
	}

}