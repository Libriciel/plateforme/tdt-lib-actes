<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\Utils\XSDValidation;

class ActesXSD {

    const ACTES_NAMESPACE = 'http://www.interieur.gouv.fr/ACTES#v1.1-20040216';
    const ACTES_NAMESPACE_PREFIX = 'actes';

    public static function getCurrentXSDPath(){
        return __DIR__ . "/xsds/current/actesv1_1.xsd";
    }

    public static function getOldXSDPaths(){
        return [__DIR__ . "/xsds/old/actesv1_1.xsd",__DIR__ . "/xsds/old/actesv1_1-new.xsd"];
    }

    public static function getXSD(){
        return file_get_contents(self :: getCurrentXSDPath());
    }

    public function getSimpleXMLForActesContent($xml_content){
        $xml = simplexml_load_string($xml_content);
        $this->validate($xml_content);
        $xml->registerXPathNamespace(
            self::ACTES_NAMESPACE_PREFIX,
            self::ACTES_NAMESPACE
        );
        return $xml;
    }

    public function validate($xml){
        $xsdValidation = new XSDValidation();
        try {
            return $xsdValidation->validate($xml, $this->getCurrentXSDPath());
        } catch (\Exception $e){
            $error_to_throw = $e;
        }
        foreach ($this->getOldXSDPaths() as $path) {
            try {
                return $xsdValidation->validate($xml, $path);
            } catch (\Exception $e) {
                throw $error_to_throw;
            }
        }
    }
}