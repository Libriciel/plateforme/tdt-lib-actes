<?php

namespace Libriciel\LibActes;

use Exception;
use SimpleXMLElement;

class PieceJointeXML
{
    /**
     * @var false|\SimpleXMLElement
     */
    private $xml;

    /**
     * @throws Exception
     */
    public function __construct(string $filename){
        $this->xml = simplexml_load_file($filename,SimpleXMLElement::class,LIBXML_PARSEHUGE);
        if (! $this->xml){
            throw new Exception("Le document n'est pas un document XML");
        }
    }

    public function hasScellement(): bool
    {
        if($this->isCompteFinancierUnique()){
            return isset($this->xml->DocumentBudgetaire->Scellement);
        }
        return isset($this->xml->Scellement);
    }

    /**
     * @return bool
     */
    public function isCompteFinancierUnique(): bool
    {
        return $this->xml->getName() === "CompteFinancierUnique";
    }

}