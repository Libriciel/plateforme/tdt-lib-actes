<?php

namespace Libriciel\LibActes;

use Libriciel\LibActes\FichierXML\EnveloppeAnomalie;
use Libriciel\LibActes\FichierXML\EnveloppeMISILLCL;
use Libriciel\LibActes\FichierXML\FichierXML;
use Libriciel\LibActes\FichierXML\MessageMetierActes;
use Libriciel\LibActes\FichierXML\MessageMetierAnnulation;
use Libriciel\LibActes\FichierXML\MessageMetierARActes;
use Libriciel\LibActes\FichierXML\EnveloppeCLMISILL;
use Libriciel\LibActes\FichierXML\MessageMetieAnomalieActe;
use Libriciel\LibActes\FichierXML\MessageMetierARAnnulation;
use Libriciel\LibActes\FichierXML\MessageMetierARDemandePieceComplementaire;
use Libriciel\LibActes\FichierXML\MessageMetierARLettreObservations;
use Libriciel\LibActes\FichierXML\MessageMetierARPieceComplementaire;
use Libriciel\LibActes\FichierXML\MessageMetierARReponseRejetLettreObservations;
use Libriciel\LibActes\FichierXML\MessageMetierCourrierSimple;
use Libriciel\LibActes\FichierXML\MessageMetierDefereTA;
use Libriciel\LibActes\FichierXML\MessageMetierDemandeClassification;
use Libriciel\LibActes\FichierXML\MessageMetierDemandePieceComplementaire;
use Libriciel\LibActes\FichierXML\MessageMetierLettreObservations;
use Libriciel\LibActes\FichierXML\MessageMetierPieceComplementaire;
use Libriciel\LibActes\FichierXML\MessageMetierRefusPieceComplementaire;
use Libriciel\LibActes\FichierXML\MessageMetierRejetLettreObservations;
use Libriciel\LibActes\FichierXML\MessageMetierReponseClassificationSansChangement;
use Libriciel\LibActes\FichierXML\MessageMetierReponseCourrierSimple;
use Libriciel\LibActes\FichierXML\MessageMetierReponseLettreObservations;
use Libriciel\LibActes\FichierXML\MessageMetierRetourClassification;
use Libriciel\LibActes\Utils\XMLGenerator;
use Libriciel\LibActes\Utils\XpathPropertiesValuesMapper;


class ActesXML {

    public function getXML(FichierXML $actesData){
        $xmlGenerator = new XMLGenerator();
        $actesXSD = new ActesXSD();
        return $xmlGenerator->createXMLfromXSD(
            $actesXSD->getXSD(),
            $this->getXPathMapping($actesData)
        );
    }


    public function getFileListe(FichierXML $fichierXML){
        $num_fichier  = 1;

        $result = array();
        foreach($fichierXML->getFileList() as $filekey){
            if (is_array($fichierXML->$filekey)){
                foreach($fichierXML->$filekey as $i => $filepath){
                    $fkey = & $fichierXML->$filekey;
                    $r = $this->getNomFichier(
                        $fichierXML,
                        $fkey[$i],
                        $num_fichier++
                    );
                    $result[$r] = $filepath;
                }
            } else {
                $r = $this->getNomFichier(
                    $fichierXML,
                    $fichierXML->$filekey,
                    $num_fichier++
                );
                $result[$r] = $fichierXML->$filekey;
            }
        }
        return $result;
    }

    private function getXPathMapping(FichierXML $actesData){

        $actesDataResult = clone $actesData;

        foreach($actesDataResult->getSpecialValue() as $key => $value){
            $actesDataResult->$key = $value;
        }

        $num_fichier  = 1;
        foreach($actesDataResult->getFileList() as $filekey){
            if (is_array($actesDataResult->$filekey)){
                foreach($actesDataResult->$filekey as $i => $filepath){
                    $fkey = & $actesDataResult->$filekey;
                    $fkey[$i] = $this->getNomFichier(
                        $actesDataResult,
                        $fkey[$i],
                        $num_fichier++
                    );
                }
            } else {
                $actesDataResult->$filekey = $this->getNomFichier(
                    $actesDataResult,
                    $actesDataResult->$filekey,
                    $num_fichier++
                );
            }
        }

        $xpathPropertiesValuesMapper = new XpathPropertiesValuesMapper();
        $xpath_mapping = $xpathPropertiesValuesMapper->getMappingValues(
            $actesData->getPropertieMapping(),
            $actesDataResult
        );

        return $xpath_mapping;
    }

    public function getDataFromFile($filepath){
        $fileXML = $this->getDataFromXML(file_get_contents($filepath));
        $fileXML->file_path = $filepath;

        $messageMetierFilename = new MessageMetierFilename();
        $info = $messageMetierFilename->getInfoFromEnvelloppeName(basename($filepath));

        $fileXML->siren = $info['siren'];
        $fileXML->departement = $info['departement'];

        if (! $fileXML->code_nature_numerique && $info['nature_acte']) {
            $fileXML->code_nature_numerique = $info['nature_acte'];
        }
        if (! $fileXML->numero_interne && $info['numero_acte']) {
            $fileXML->numero_interne = $info['numero_acte'];
        }
        if (! $fileXML->date_actes && $info['numero_acte']) {
            $fileXML->date_actes = $info['date_acte'];
        }
        return $fileXML;
    }


    public function getDataFromXML($xml_content){
        $actesXMLHelper = new ActesXSD();
        $xml = $actesXMLHelper->getSimpleXMLForActesContent($xml_content);

        switch ($xml->getName()){
            case "EnveloppeCLMISILL" : $actesData = new EnveloppeCLMISILL(); break;
            case "EnveloppeMISILLCL" : $actesData = new EnveloppeMISILLCL(); break;
            case "AnomalieEnveloppe" : $actesData = new EnveloppeAnomalie(); break;
            case "Acte" : $actesData = new MessageMetierActes(); break;
            case "ARActe" : $actesData = new MessageMetierARActes(); break;
            case "AnomalieActe" : $actesData = new MessageMetieAnomalieActe(); break;
            case "CourrierSimple" : $actesData = new MessageMetierCourrierSimple(); break;
            case "ReponseCourrierSimple": $actesData = new MessageMetierReponseCourrierSimple(); break;
            case "DemandePieceComplementaire": $actesData = new MessageMetierDemandePieceComplementaire(); break;
            case "ARDemandePieceComplementaire": $actesData = new MessageMetierARDemandePieceComplementaire(); break;
            case "RefusPieceComplementaire": $actesData = new MessageMetierRefusPieceComplementaire(); break;
            case "PieceComplementaire": $actesData = new MessageMetierPieceComplementaire(); break;
            case "ARPieceComplementaire": $actesData = new MessageMetierARPieceComplementaire(); break;
            case "LettreObservations": $actesData = new MessageMetierLettreObservations(); break;
            case "ARLettreObservations": $actesData = new MessageMetierARLettreObservations(); break;
            case "RejetLettreObservations": $actesData = new MessageMetierRejetLettreObservations(); break;
            case "ReponseLettreObservations": $actesData = new MessageMetierReponseLettreObservations(); break;
            case "ARReponseRejetLettreObservations": $actesData = new MessageMetierARReponseRejetLettreObservations(); break;
            case "DefereTA": $actesData = new MessageMetierDefereTA(); break;
            case "Annulation": $actesData = new MessageMetierAnnulation(); break;
            case "ARAnnulation": $actesData = new MessageMetierARAnnulation(); break;
            case "DemandeClassification": $actesData = new MessageMetierDemandeClassification(); break;
            case "RetourClassification": $actesData = new MessageMetierRetourClassification(); break;
            case "ReponseClassificationSansChangement": $actesData = new MessageMetierReponseClassificationSansChangement(); break;

            default:
                throw new \Exception("{$xml->getName()} non géré par tdt-lib-acte");
        }

        $this->setPropertiesWithMapping(
            $actesData->getPropertieMapping(),
            $xml,
            $actesData
        );

        foreach($actesData->getMandatoryField() as $mandatory_field){
            if (! $actesData->$mandatory_field){
                throw new \Exception("Le champs $mandatory_field est obligatoire");
            }
        }
        return  $actesData;
    }

    private function getNomFichier(FichierXML $transmissionData,
                                  $original_filepath,
                                  $num_fichier
    ){
        $messageMetierFilename = new MessageMetierFilename();
        return $messageMetierFilename-> getNomFichier($transmissionData,
                                  $original_filepath,
                                  $num_fichier);
    }

    private function setPropertiesWithMapping(
        array $properties_mapping,
        \SimpleXMLElement $xml,
        $object
    )
    {
        foreach ($properties_mapping as $xpath => $propertie) {
            $xpath = preg_replace('#\[\]#', "", $xpath);
            $xpath_result = $xml->xpath($xpath);
            if (!$xpath_result) {
                continue;
            }
            $prop = & $object->$propertie;
            if (count($xpath_result) > 1) {
                foreach ($xpath_result as $i => $value) {
                    $prop[$i] = $this->getValue($value);
                }
            } else {
                if (isset($object->$propertie) && is_array($object->$propertie)){

                    $prop[0] = $this->getValue($xpath_result);
                } else {
                    $object->$propertie = $this->getValue($xpath_result);
                }
            }
        }
    }

    private function getValue($XMLElement){
        $value = strval($XMLElement[0]);

        if (strval(intval($value)) === $value) {
            $value = intval($value);
        }
        return $value;
    }
}