<?php

namespace Libriciel\LibActes;

class ArchiveFilename {

	public function getInfo($archive_filename){
		$result = preg_match("#^([^-]*)-(.*)\.tar\.gz$#", $archive_filename,$matches);

		if (! $result){
			throw new \Exception("Le nom du fichier contenant l'archive ($archive_filename) est incorrect");
		}

		$r['itc'] = $matches[1];
		$r['enveloppe_filename'] = $matches[2].".xml";
		return $r;
	}

	public function getFilename($identifiant_dispositif,$enveloppe_metier_filename){
	    $part_enveloppe = pathinfo($enveloppe_metier_filename, PATHINFO_FILENAME);
        return "{$identifiant_dispositif}-$part_enveloppe.tar.gz";
    }

}