<?php

//TODO il faudrait utiliser les fonction file_get_content et DOM qui ne génère pas de warning...
error_reporting(E_ERROR | E_PARSE);

require_once __DIR__."/../vendor/autoload.php";

$RED="\033[0;31m";
$GREEN="\033[0;32m";
$NC="\033[0m"; # No Color


if ($argc<3){
	echo "Usage {$argv[0]} TRIGRAMME QUADRIGRAMME directory\n";
	echo "Valide un ensemble d'archive généré par generation-enveloppe-actes-test\n";
	exit(-2);
}

$actes_appli_trigramme = $argv[1];
$actes_appli_quadrigramme = $argv[2];
$directory = $argv[3];


$archive = new \Libriciel\LibActes\ArchiveValidator($actes_appli_trigramme,$actes_appli_quadrigramme);

exec("find $directory -name '*.tar.gz'",$output);

$result = [];

echo "Analyse des enveloppes...\n";


foreach($output as $file){
	echo "$file : ";
	try {
		$archive->validate($file);
	} catch (Exception $e){
		$result[$file] = false;
		echo "${RED}Erreur lors de la validation de l'archive : ".$e->getMessage()."${NC}\n";
		continue;
	}
	$result[$file] = true;
	echo "${GREEN}Archive valide${NC}\n";
}

echo "\nComparaison avec les résultats attendus\n";


foreach($result as $file=>$actual){
	preg_match("#-(\d+).tar.gz$#",$file,$matches);
	$result_actual[$matches[1]] = boolval($actual);
}
ksort($result_actual);




$rapport = file_get_contents($directory."/rapportnormes");

$rapport = json_decode($rapport,true);

echo "\nFichier\trésultat attendu\trésultat : \n\n";

foreach($rapport as $file=>$expected){
	preg_match("#-(\d+)\..{6}$#",$file,$matches);
	$result_expected[$matches[1]] = boolval(preg_match('#acceptee#',$expected));
	echo "$file\t".($expected=="acceptee"?"${GREEN}accepte${NC}":"${RED}rejetee${NC}")."\t".($result_actual[$matches[1]]?"${GREEN}accepte${NC}":"${RED}rejetee${NC}")."\n";
}
ksort($result_expected);


if ($result_actual == $result_expected){
	echo "\n${GREEN}Toutes les analyses d'archives sont conformes !${NC}\n";
	exit(0);
} else {
	echo "\n${RED}Certaines analyses d'archive ne sont pas conformes !${NC}\n";
	exit(-1);
}



