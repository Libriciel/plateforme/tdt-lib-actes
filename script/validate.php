<?php

require_once __DIR__."/../vendor/autoload.php";

if ($argc<3){
	echo "Usage {$argv[0]} TRIGRAMME archive.tar.gz\n";
	echo "Valide une archive @ctes\n";
	exit(-2);
}

$actes_appli_trigramme = $argv[1];
$archive_filepath = $argv[2];

$archive = new \Libriciel\LibActes\ArchiveValidator($actes_appli_trigramme);

try {
	$archive->validate($archive_filepath);
} catch (Exception $e){
	echo "Erreur lors de la validation de l'archive : ".$e->getMessage()."\n";
	exit(-1);
}

echo "L'archive est valide\n";

exit(0);