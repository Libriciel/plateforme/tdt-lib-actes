<?php

use Libriciel\LibActes\Utils\XpathPropertiesValuesMapper;

class SimpleObject2
{
    public $var_name;
}
class XpathPropertiesValuesMapperTest extends PHPUnit_Framework_TestCase
{
    public function testSimpleObject()
    {
        error_reporting(E_ALL); // On veut récupérer aussi les deprecation notices
        $xpathPropertiesValuesMapper = new XpathPropertiesValuesMapper();

        $mapping = [
            "racine/test/@actes:Departement"=>"var_name"
        ];
        $object = new SimpleObject2();
        $object->var_name="var_value";
        $this->assertEquals(
            ['racine'=>['test'=>['@actes:Departement'=>'var_value']]],
            $xpathPropertiesValuesMapper->getMappingValues($mapping,$object)
        );

    }
}