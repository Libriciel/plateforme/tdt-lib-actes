<?php

use Libriciel\LibActes\ActesXML;

class ClassificationTest extends PHPUnit_Framework_TestCase {

    public function testClassification(){

        $actesXML = new ActesXML();
        try {
            $actesXML->getDataFromXML(file_get_contents(__DIR__ . "/../../src/classification/classification.xml"));
        } catch (\Libriciel\LibActes\Utils\XSDValidationException $e){
            print_r($e->getValidationErrors());
            throw $e;
        }

        $actesXML->getDataFromXML(file_get_contents(__DIR__."/../../src/classification/classification-acte2-ancienne-notice.xml"));
		$actesXML->getDataFromXML(file_get_contents(__DIR__."/../../src/classification/classification-before-actes-2.xml"));
        $this->assertTrue(true); //No exception
    }

}