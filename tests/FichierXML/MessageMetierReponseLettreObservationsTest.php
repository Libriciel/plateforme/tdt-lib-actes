<?php

use Libriciel\LibActes\FichierXML\MessageMetierReponseLettreObservations;


class MessageMetierReponseLettreObservationsTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetier = new MessageMetierReponseLettreObservations();
        $messageMetier->id_actes = "032-213201601-20170616-ARP201706407-AI";
        $messageMetier->date_courrier_pref = "2017-06-17";
        $messageMetier->document = __DIR__."/fixtures/deliberation.pdf";

        $messageMetier->siren = "000000000";
        $messageMetier->departement = "001";
        $messageMetier->code_nature_numerique = 1;
        $messageMetier->date_actes = "2017-01-30";

        $messageMetier->numero_interne = "TEST42";

        return $messageMetier;
    }

    public function getFichierXMLResult() {
        $result = $this->getFichierXML();
        $result->document = "001-000000000-20170130-TEST42-DE-4-4_1.pdf";
        return $result;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-4-4_0.xml";
    }

    public function getFileList() {
        return array(__DIR__."/fixtures/deliberation.pdf");
    }

    public function getReponseType(){
        return "\Libriciel\LibActes\FichierXML\MessageMetierARReponseRejetLettreObservations";
    }

    public function getReponseFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-4-5_0.xml";
    }
}