<?php

use Libriciel\LibActes\FichierXML\MessageMetierReponseClassificationSansChangement;


class MessageMetierReponseClassificationSansChangementTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetier = new MessageMetierReponseClassificationSansChangement();

        $messageMetier->siren = "000000000";
        $messageMetier->departement = "001";
        $messageMetier->date_classification = "1984-01-01";

        return $messageMetier;
    }

    public function getFichierXMLResult() {
        $result = $this->getFichierXML();
        return $result;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000----7-3_0.xml";
    }

    public function getFileList() {
        return array();
    }
}