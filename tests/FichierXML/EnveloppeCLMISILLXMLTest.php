<?php

use Libriciel\LibActes\FichierXML\EnveloppeCLMISILL;
use Libriciel\LibActes\ActesXML;


class EnveloppeCLMISILLXMLTest extends FichierXMLTesting {

    public function getFichierXML() {
        $enveloppeCLMISILLData = new EnveloppeCLMISILL();

        $enveloppeCLMISILLData->departement="012";
        $enveloppeCLMISILLData->arrondissement=1;
        $enveloppeCLMISILLData->siren="000000000";
        $enveloppeCLMISILLData->nature= "01";
        $enveloppeCLMISILLData->adresses_retour = array(
            "toto@libriciel.fr",
            "titi@libriciel.fr"
        );
        $enveloppeCLMISILLData->email_referent = "foo@libricile.fr";
        $enveloppeCLMISILLData->nom_referent = "foo";
        $enveloppeCLMISILLData->telephone_referent = "00 00 00 00 00";

        $enveloppeCLMISILLData->fichier = array(
            "toto.xml", "titi.xml"
        );
        $enveloppeCLMISILLData->signature = array("xxx","zzz");
        return $enveloppeCLMISILLData;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/EnveloppeCLMISILL.xml";
    }

    public function testGetXML(){
        $messageMetierActesXML = new ActesXML();
        $fichierXML = $messageMetierActesXML->getDataFromXML(file_get_contents($this->getFixtureFile()));
        $messageMetierActesData_orig = $this->getFichierXML();
        $this->assertEquals($messageMetierActesData_orig,$fichierXML);
    }

    public function testGenerateTransmission() {
        $this->assertTrue(true);
    }

    public function getReponseType(){
        return "\Libriciel\LibActes\FichierXML\EnveloppeMISILLCL";
    }

    public function getReponseFile(){
        return __DIR__ . "/fixtures/EnveloppeMISILLCL.xml";
    }

}