<?php

use Libriciel\LibActes\FichierXML\EnveloppeAnomalie;
use Libriciel\LibActes\ActesXML;


class EnveloppeAnomalieTest extends FichierXMLTesting {

    public function getFichierXML(): EnveloppeAnomalie
    {
        $enveloppeAnomalie = new EnveloppeAnomalie();

        $enveloppeAnomalie->detail_erreur = "Erreur de test";
        $enveloppeAnomalie->nature_erreur = 5;
        $enveloppeAnomalie->date = "2017-06-12";

        $enveloppeAnomalie->departement="012";
        $enveloppeAnomalie->arrondissement=1;
        $enveloppeAnomalie->siren="000000000";
        $enveloppeAnomalie->nature= "01";
        $enveloppeAnomalie->adresses_retour = array(
            "toto@libriciel.fr",
            "titi@libriciel.fr"
        );
        $enveloppeAnomalie->email_referent = "foo@libricile.fr";
        $enveloppeAnomalie->nom_referent = "foo";
        $enveloppeAnomalie->telephone_referent = "00 00 00 00 00";

        $enveloppeAnomalie->fichier = array(
            "toto.xml", "titi.xml"
        );
        $enveloppeAnomalie->signature = array("xxx","zzz");
        return $enveloppeAnomalie;
    }

    public function getFixtureFile(): string
    {
        return __DIR__ . "/fixtures/EnveloppeAnomalie.xml";
    }

    public function testGetXML(){
        $messageMetierActesXML = new ActesXML();
        $fichierXML = $messageMetierActesXML->getDataFromXML(file_get_contents($this->getFixtureFile()));
        $messageMetierActesData_orig = $this->getFichierXML();
        $this->assertEquals($messageMetierActesData_orig,$fichierXML);
    }

    public function testGenerateTransmission() {
        $this->assertTrue(true);
    }

}