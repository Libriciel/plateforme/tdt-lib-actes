<?php

use Libriciel\LibActes\FichierXML\MessageMetieAnomalieActe;
use Libriciel\LibActes\Utils\ObjectCopy;

require_once __DIR__."/MessageMetierActesTest.php";

class MessageMetierAnomalieActeTest extends FichierXMLTesting{

    public function getFichierXML(){
        $messageMetierAnomalieActe = new MessageMetieAnomalieActe();


        $messageMetierAnomalieActe->date_anomalie = "2017-06-16";
        $messageMetierAnomalieActe->nature_anomalie = "042";
        $messageMetierAnomalieActe->detail_anomalie = "Ca ne fonctionne pas";
        $messageMetierAnomalieActe->classification_date_version_en_cours = "2014-05-10";


        $messageMetierActesTest = new MessageMetierActesTest();
        $messageMetierActesData = $messageMetierActesTest->getFichierXMLResult();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($messageMetierActesData,$messageMetierAnomalieActe);
        return $messageMetierAnomalieActe;
    }



    public function getFixtureFile(){
        return __DIR__."/fixtures/001-000000000----1-3_0.xml";
    }
}