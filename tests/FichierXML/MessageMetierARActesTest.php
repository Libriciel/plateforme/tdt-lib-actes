<?php

use Libriciel\LibActes\FichierXML\MessageMetierARActes;
use Libriciel\LibActes\Utils\ObjectCopy;

require_once __DIR__."/MessageMetierActesTest.php";

class MessageMetierARActesTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetierActesTest = new MessageMetierActesTest();
        $messageMetierActesData = $messageMetierActesTest->getFichierXMLResult();

        $messageMetierARActes = new MessageMetierARActes();
        $objectCopy = new ObjectCopy();
        $objectCopy->copy($messageMetierActesData,$messageMetierARActes);

        $messageMetierARActes->id_actes = "001-000000000-20170130-TEST42-DE";
        $messageMetierARActes->date_reception="2017-06-16";
        $messageMetierARActes->classification_date_version_en_cours = "2014-01-02";
        return $messageMetierARActes;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-1-2_0.xml";
    }


}