<?php

use Libriciel\LibActes\FichierXML\MessageMetierPieceComplementaire;


class MessageMetierPieceComplementaireTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetier = new MessageMetierPieceComplementaire();
        $messageMetier->id_actes = "032-213201601-20170616-ARP201706407-AI";
        $messageMetier->date_courrier_pref = "2017-06-17";
        $messageMetier->document = array(
            __DIR__."/fixtures/deliberation.pdf",
            __DIR__."/fixtures/deliberation.pdf"
        );

        $messageMetier->siren = "000000000";
        $messageMetier->departement = "001";
        $messageMetier->code_nature_numerique = 1;
        $messageMetier->date_actes = "2017-01-30";

        $messageMetier->numero_interne = "TEST42";

        return $messageMetier;
    }

    public function getFichierXMLResult() {
        $result = $this->getFichierXML();
        $result->document = array(
            "001-000000000-20170130-TEST42-DE-3-4_1.pdf",
            "001-000000000-20170130-TEST42-DE-3-4_2.pdf",
        );
        return $result;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-3-4_0.xml";
    }

    public function getFileList() {
        return array(__DIR__."/fixtures/deliberation.pdf");
    }

    public function getReponseType(){
        return "\Libriciel\LibActes\FichierXML\MessageMetierARPieceComplementaire";
    }

    public function getReponseFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-3-5_0.xml";
    }
}