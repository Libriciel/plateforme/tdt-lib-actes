<?php

use Libriciel\LibActes\FichierXML\EnveloppeMISILLCL;
use Libriciel\LibActes\ActesXML;


class EnveloppeMISILLCLXMLTest extends FichierXMLTesting {

    public function getFichierXML() {
        $enveloppeMISILLCL = new EnveloppeMISILLCL();

        $enveloppeMISILLCL->departement_spref="012";
        $enveloppeMISILLCL->arrondissement_spref=1;


        $enveloppeMISILLCL->siren="000000000";

        $enveloppeMISILLCL->email_referent = "foo@libricile.fr";
        $enveloppeMISILLCL->nom_referent = "foo";
        $enveloppeMISILLCL->telephone_referent = "00 00 00 00 00";

        $enveloppeMISILLCL->fichier = array(
            "toto.xml", "titi.xml"
        );
        $enveloppeMISILLCL->signature = array("xxx","zzz");
        return $enveloppeMISILLCL;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/EnveloppeMISILLCL.xml";
    }

    public function testGetXML(){
        $messageMetierActesXML = new ActesXML();
        $fichierXML = $messageMetierActesXML->getDataFromXML(file_get_contents($this->getFixtureFile()));
        $messageMetierActesData_orig = $this->getFichierXML();
        $this->assertEquals($messageMetierActesData_orig,$fichierXML);
    }

    public function testGenerateTransmission() {
        $this->assertTrue(true);
    }

}