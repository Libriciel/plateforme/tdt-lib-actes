<?php

use Libriciel\LibActes\FichierXML\MessageMetierAnnulation;


class MessageMetierAnnulationTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetier = new MessageMetierAnnulation();
        $messageMetier->id_actes = "032-213201601-20170616-ARP201706407-AI";

        $messageMetier->siren = "000000000";
        $messageMetier->departement = "001";
        $messageMetier->code_nature_numerique = 1;
        $messageMetier->date_actes = "2017-01-30";

        $messageMetier->numero_interne = "TEST42";

        return $messageMetier;
    }

    public function getFichierXMLResult() {
        $result = $this->getFichierXML();
        return $result;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-6-1_0.xml";
    }

    public function getFileList() {
        return array();
    }

    public function getReponseType(){
        return "\Libriciel\LibActes\FichierXML\MessageMetierARAnnulation";
    }

    public function getReponseFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-6-2_0.xml";
    }
}