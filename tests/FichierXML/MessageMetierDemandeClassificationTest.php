<?php

use Libriciel\LibActes\FichierXML\MessageMetierDemandeClassification;


class MessageMetierDemandeClassificationTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetier = new MessageMetierDemandeClassification();

        $messageMetier->siren = "000000000";
        $messageMetier->departement = "001";
        $messageMetier->date_classification = "1984-01-01";

        return $messageMetier;
    }

    public function getFichierXMLResult() {
        $result = $this->getFichierXML();
        return $result;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000----7-1_0.xml";
    }

    public function getFileList() {
        return array();
    }
}