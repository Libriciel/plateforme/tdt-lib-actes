<?php

use Libriciel\LibActes\FichierXML\MessageMetierLettreObservations;


class MessageMetierLettreObservationsTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetier = new MessageMetierLettreObservations();
        $messageMetier->id_actes = "032-213201601-20170616-ARP201706407-AI";
        $messageMetier->date_lettre_observation = "2017-06-17";
        $messageMetier->document = __DIR__."/fixtures/deliberation.pdf";
        $messageMetier->motif = "parce que je le veau bien";

        $messageMetier->siren = "000000000";
        $messageMetier->departement = "001";
        $messageMetier->code_nature_numerique = 1;
        $messageMetier->date_actes = "2017-01-30";

        $messageMetier->numero_interne = "TEST42";

        return $messageMetier;
    }

    public function getFichierXMLResult() {
        $result = $this->getFichierXML();
        $result->document = "001-000000000-20170130-TEST42-DE-4-1_1.pdf";
        return $result;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-4-1_0.xml";
    }

    public function getFileList() {
        return array(__DIR__."/fixtures/deliberation.pdf");
    }

    public function getReponseType(){
        return "\Libriciel\LibActes\FichierXML\MessageMetierARLettreObservations";
    }

    public function getReponseFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST42-DE-4-2_0.xml";
    }
}