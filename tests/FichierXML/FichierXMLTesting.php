<?php

use Libriciel\LibActes\ActesXML;
use Libriciel\LibActes\Utils\XSDValidationException;
use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\TransmissionGenerator;
use Libriciel\LibActes\FichierXML\FichierXML;
use PHPUnit\Framework\TestCase;

abstract class FichierXMLTesting extends TestCase {

    /**
     * @return FichierXML
     */
    abstract public function getFichierXML();

    abstract public function getFixtureFile();

    public function getReponseType(){
        return false;
    }

    public function getFichierXMLResult(){
        return $this->getFichierXML();
    }

    public function getFileList(){
        return array();
    }

    public function getReponseFile(){return false;}

    public function testGenerate(){
        $messageMetierARActes = $this->getFichierXML();
        $messageMetierActesXML = new ActesXML();

        $xml = $messageMetierActesXML->getXML($messageMetierARActes);
        //echo $xml;
        $actesXSD = new \Libriciel\LibActes\ActesXSD();
        try {
            $result = $actesXSD->validate($xml);
        } catch(XSDValidationException $XSDValidationException){
            print_r($XSDValidationException->displayValidationErrors());
            throw $XSDValidationException;
        }

        $this->assertTrue($result);

        if (! file_exists($this->getFixtureFile())){
            echo $xml;
            //file_put_contents($this->getFixtureFile(),$xml);
        }

        $this->assertStringEqualsFile(
            $this->getFixtureFile(),
            $xml
        );
    }

    public function testGetXML(){
        $actesXML = new ActesXML();
        $fichierXML = $actesXML->getDataFromFile($this->getFixtureFile());
        $messageMetierActesData_orig = $this->getFichierXMLResult();
        unset($fichierXML->file_path);
        unset($messageMetierActesData_orig->file_path);
        $this->assertEquals($messageMetierActesData_orig,$fichierXML);
    }

    public function testGenerateTransmission(){
        $fichierXML = $this->getFichierXML();

        $tmpDir = new TmpDir();
        $tmp_dir =$tmpDir->getTmpDir();

        $transmissionGenerator = new TransmissionGenerator();
        $message_metier_filepath = $transmissionGenerator->generateFiles(
            $fichierXML,
            $tmp_dir
        );

        $this->assertFileExists($message_metier_filepath);
        $this->assertFileEquals(
            $this->getFixtureFile(),
            $message_metier_filepath
        );

        $xml = simplexml_load_file($message_metier_filepath);

        $xml->registerXPathNamespace(
            'actes',
            'http://www.interieur.gouv.fr/ACTES#v1.1-20040216'
        );

        $node = $xml->xpath("//actes:NomFichier") ;

        foreach ($this->getFileList() as $i => $file){
            $this->assertFileEquals(
                $file,
                $tmp_dir."/".strval($node[$i])
            );
        }

        $fichierXML_result = $transmissionGenerator->getTransmissionData($message_metier_filepath);

        $fichierXML_expected = $this->getFichierXMLResult();

        $tr1 = clone $fichierXML_expected;
        $tr2 = clone $fichierXML_result;

        $file_list = $fichierXML->getFileList();
        foreach($file_list as $filekey){
            unset($tr1->$filekey);
            unset($tr2->$filekey);
        }
        unset($tr1->file_path);
        unset($tr2->file_path);
        $this->assertEquals($tr1,$tr2);
        $tmpDir->destruct();
    }

    public function getSens(){
        $this->assertTrue(
            in_array($this->getFichierXML()->getSens(),array(FichierXML::SENS_MI_CL,FichierXML::SENS_CL_MI))
        );
    }

    public function testGetAR(){
        $fichierXMLRecu = $this->getFichierXMLResult();
        $fichierXMLReponse = $fichierXMLRecu->getReponse("2017-06-16");


        if ( !$this->getReponseType()){
            $this->assertFalse($fichierXMLReponse);
            return;
        }

        $this->assertInstanceOf($this->getReponseType(),$fichierXMLReponse);

        $actesXML = new \Libriciel\LibActes\ActesXML();
        $xml =  $actesXML->getXML($fichierXMLReponse);

        $actesXSD = new \Libriciel\LibActes\ActesXSD();

        try {
            $actesXSD->validate($xml);
        } catch (\Libriciel\LibActes\Utils\XSDValidationException $e){
            print_r($e->getValidationErrors());
        }
        $this->assertStringEqualsFile($this->getReponseFile(),$xml);
    }


}