<?php

use Libriciel\LibActes\FichierXML\MessageMetierRetourClassification;


class MessageMetierRetourClassificationTest extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetier = new MessageMetierRetourClassification();

        $messageMetier->siren = "000000000";
        $messageMetier->departement = "001";
        $messageMetier->date_classification = "1984-01-01";
        $messageMetier->nature_actes_code = array("1","2");
        $messageMetier->nature_actes_libelle = array("test1","test2");
        $messageMetier->nature_actes_type_abrege = array("T1","T2");
        $messageMetier->matiere1_code = array("1");
        $messageMetier->matiere1_libelle = array("foo1");
        $messageMetier->matiere2_code = array(1,2);
        $messageMetier->matiere2_libelle = array("bar1","bar2");
        return $messageMetier;
    }

    public function getFichierXMLResult() {
        $result = $this->getFichierXML();
        return $result;
    }

    public function getFixtureFile(){
        return __DIR__ . "/fixtures/001-000000000----7-2_0.xml";
    }

    public function getFileList() {
        return array();
    }
}