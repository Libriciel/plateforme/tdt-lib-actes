<?php

use Libriciel\LibActes\FichierXML\MessageMetierActes;

class MessageMetierActesTestNew extends FichierXMLTesting {

    public function getFichierXML(){
        $messageMetierActesData = new MessageMetierActes();

        $messageMetierActesData->siren = "000000000";
        $messageMetierActesData->departement = "001";

        $messageMetierActesData->actes_filepath = __DIR__."/fixtures/deliberation.pdf";
        $messageMetierActesData->annexe_filepath = array(
            __DIR__."/fixtures/tuxapple.png",
            __DIR__."/fixtures/deliberation.pdf"
        );
        $messageMetierActesData->classification_date_version = "2014-01-02";
        $messageMetierActesData->code_nature_numerique = 1;
        $messageMetierActesData->date_actes = "2017-01-30";

        $messageMetierActesData->numero_interne = "TEST43";

        $messageMetierActesData->objet = "test de télétransmission";

        $messageMetierActesData->classification_1 = 1;
        $messageMetierActesData->classification_2 = 3;
        $messageMetierActesData->classification_3 = 4;

        $messageMetierActesData->document_papier = 'O';

        return $messageMetierActesData;
    }

    public function getFichierXMLResult(){
        $messageMetierActesData = new MessageMetierActes();

        $messageMetierActesData->siren = "000000000";
        $messageMetierActesData->departement = "001";

        $messageMetierActesData->actes_filepath = "001-000000000-20170130-TEST43-DE-1-1_1.pdf";
        $messageMetierActesData->annexe_filepath = array(
            "001-000000000-20170130-TEST43-DE-1-1_2.png",
            "001-000000000-20170130-TEST43-DE-1-1_3.pdf"
        );
        $messageMetierActesData->classification_date_version = "2014-01-02";
        $messageMetierActesData->code_nature_numerique = 1;
        $messageMetierActesData->date_actes = "2017-01-30";

        $messageMetierActesData->numero_interne = "TEST43";

        $messageMetierActesData->objet = "test de télétransmission";

        $messageMetierActesData->classification_1 = 1;
        $messageMetierActesData->classification_2 = 3;
        $messageMetierActesData->classification_3 = 4;

        $messageMetierActesData->{'nombre_annexes'} = 2;
        $messageMetierActesData->document_papier = 'O';
        return $messageMetierActesData;
    }

    public function getFixtureFile(){
        return __DIR__."/fixtures/001-000000000-20170130-TEST43-DE-1-1_0.xml";
    }

    public function getFileList(){
        return array(
            __DIR__."/fixtures/deliberation.pdf",
            __DIR__."/fixtures/tuxapple.png",
            __DIR__."/fixtures/deliberation.pdf"
        );
    }

    public function getReponseType(){
        return "\Libriciel\LibActes\FichierXML\MessageMetierARActes";
    }

    public function getReponseFile(){
        return __DIR__ . "/fixtures/001-000000000-20170130-TEST43-DE-1-2_0.xml";
    }

}