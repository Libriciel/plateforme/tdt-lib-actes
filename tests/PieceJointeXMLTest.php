<?php

use Libriciel\LibActes\PieceJointeXML;

class PieceJointeXMLTest extends PHPUnit_Framework_TestCase
{
    /**
     * @throws \Exception
     */
    public function testCompteFinancierUniqueWithScellement(){
        $xml = new PieceJointeXML(__DIR__."/fixtures/CFU-SCELLE.xml");
        $this->assertTrue(
            $xml->hasScellement()
        );
    }

    /**
     * @throws \Exception
     */
    public function testCompteFinancierUniqueWithoutScellement(){
        $xml = new PieceJointeXML(__DIR__."/fixtures/CFU-NON-SCELLE.xml");
        $this->assertFalse(
            $xml->hasScellement()
        );
    }
}