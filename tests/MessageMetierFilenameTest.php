<?php

class MessageMetierFilenameTest extends PHPUnit_Framework_TestCase {

    public function testGetInfoFromEnvelloppeNameFailed(){
        $messageMetierFilename = new \Libriciel\LibActes\MessageMetierFilename();
        $this->setExpectedException("Exception","Le nom du fichier enveloppe toto est mal formé.");
        $messageMetierFilename->getInfoFromEnvelloppeName("toto");
    }


    public function testGetInfoFromEnvelloppeName(){
        $messageMetierFilename = new \Libriciel\LibActes\MessageMetierFilename();
        $this->assertEquals(
            '179712708',$messageMetierFilename->getInfoFromEnvelloppeName("972-179712708-20160101-FDF24122015-CC-1-1_0.xml")['siren']
        );
    }

	public function testSpeciale(){
		$messageMetierFilename = new \Libriciel\LibActes\MessageMetierFilename();
        $this->assertEquals(
            '218500890',
		    $messageMetierFilename->getInfoFromEnvelloppeName("085-218500890-20180412-D18_056-DE-1-2_10022.xml")['siren']
        );
	}


}