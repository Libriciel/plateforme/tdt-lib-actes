<?php

class CourrierRetourTest extends PHPUnit_Framework_TestCase {

    public function codeMessageProvider() {
        return [
            [\Libriciel\LibActes\FichierXML\MessageMetierCourrierSimple::CODE_MESSAGE],
            [\Libriciel\LibActes\FichierXML\MessageMetierDemandePieceComplementaire::CODE_MESSAGE],
            [\Libriciel\LibActes\FichierXML\MessageMetierLettreObservations::CODE_MESSAGE],
            [\Libriciel\LibActes\FichierXML\MessageMetierDefereTA::CODE_MESSAGE]
        ];
    }

    /**
     * @param string $code_message
     * @dataProvider  codeMessageProvider
     */
    public function testGenerateReponse($code_message){
        $archive = new \Libriciel\LibActes\Archive();
        $archiveData = $archive->getArchiveDataFromFolder(__DIR__."/fixtures/test-archive/");

        $courrierRetour = new \Libriciel\LibActes\CourrierRetour();

        $archiveDataRetour = $courrierRetour->generateCourrierRetour(
            $archiveData,
            "001-000000000-20170130-TEST42-DE-1-1_0.xml",
            $code_message
        );

        $this->assertEquals("SPREF001",$archiveDataRetour->service_controle);

        $tmpDir = new \Libriciel\LibActes\Utils\TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $archive->generateFiles($archiveDataRetour,$tmp_dir);

        $this->assertEquals($code_message,$archiveDataRetour->fichierXML[0]::CODE_MESSAGE);
        //On revalide l'archive générée
        $this->assertNotNull($archive->getArchiveDataFromFolder($tmp_dir));

        $tmpDir->destruct();
    }

    public function testGenerateReponseDeferTA(){
        $archive = new \Libriciel\LibActes\Archive();
        $archiveData = $archive->getArchiveDataFromFolder(__DIR__."/fixtures/test-archive/");

        $courrierRetour = new \Libriciel\LibActes\CourrierRetour();

        $archiveDataRetour = $courrierRetour->generateCourrierRetour(
            $archiveData,
            "001-000000000-20170130-TEST42-DE-1-1_0.xml",
            "5-1"
        );

        $this->assertEquals("SPREF001",$archiveDataRetour->service_controle);

        $tmpDir = new \Libriciel\LibActes\Utils\TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $archive->generateFiles($archiveDataRetour,$tmp_dir);


        $this->assertRegExp(
            "#001-000000000-20170130-TEST42-DE-5-1_1.pdf#",
            file_get_contents($tmp_dir."/001-000000000-20170130-TEST42-DE-5-1_0.xml")
        );

        $tmpDir->destruct();
    }


}