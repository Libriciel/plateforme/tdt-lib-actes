<?php

use Libriciel\LibActes\ArchiveFilename;
use PHPUnit\Framework\TestCase;

class ArchiveFilenameTest extends TestCase {

	const IDENTIFIANT_TIERS_DE_CONFIANCE = "abc";

	/** @var  ArchiveFilename */
	private $archiveFilename;

	protected function setUp(): void
    {
		parent::setUp();
		$this->archiveFilename = new ArchiveFilename();
	}

	public function testOK(){
		$info = $this->archiveFilename->getInfo("abc-EACT--123456789--20021231-2.tar.gz");
		$this->assertEquals("abc",$info['itc']);
		$this->assertEquals("EACT--123456789--20021231-2.xml",$info['enveloppe_filename']);
	}

	private function validateBadname($bad_name,$exception_message){
        $this->expectException(Exception::class);
		$this->expectExceptionMessage(
			$exception_message
		);
		$this->archiveFilename->getInfo($bad_name);
	}

	public function testNotTargz(){
		$bad_name = "abc-EACT--123456789--20021231-2.tar.xml";
		$exception_message = "Le nom du fichier contenant l'archive ($bad_name) est incorrect";
		$this->validateBadname($bad_name, $exception_message);
	}
	

}