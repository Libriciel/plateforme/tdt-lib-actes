<?php

use Libriciel\LibActes\Archive;

class Norme12Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }


    public function testSirenMismatch(){
        $this->setExpectedException(
            "Exception",
            "Le format text/plain du fichier 045-214502494-20170717-D201717-DE-1-1_1.txt n'est pas autorisé"
        );
        $this->archive->getArchiveData(__DIR__."/unauthorized-fileformat/EACT--214502494--20170717-5.xml");
    }


}