<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme05Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }

    public function testSameTdt(){
        $archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO');
        $this->assertTrue($archiveValidator->validate(__DIR__."/../SLO-EACT--SPREF0626-246201149-20170717-4675.tar.gz"));
    }

    public function testNotSameTdt(){
        $archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SL2');
        $this->setExpectedException("Exception","L'identifiant du tdt de l'archive SLO ne correspond pas à l'identifiant courant SL2");
        $archiveValidator->validate(__DIR__."/../SLO-EACT--SPREF0626-246201149-20170717-4675.tar.gz");
    }

    public function testNameMismatch(){
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();

        try {
            $archiveData = $this->archive->getArchiveDataFromTarball(
                __DIR__ . "/SLO-EACT--SPREF0626-246201159-20170717-4675.tar.gz",
                $tmp_dir
            );
        } catch(Exception $e){
            $this->assertEquals(
                "L'enveloppe métier EACT--SPREF0626-246201159-20170717-4675.xml n'a pas été trouvé dans l'archive",
                $e->getMessage()
            );
        }

        $tmpDir->destruct();
    }



}