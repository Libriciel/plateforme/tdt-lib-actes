<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme11Test extends PHPUnit_Framework_TestCase {

	/** @var  Archive */
	protected $archive;

	public function setUp(): void
    {
		$this->archive = new Archive();
	}


	/**
	 * @throws Exception
	 */
	public function testBadPDFName(){
		$this->setExpectedException(
			"Exception",
			"Le nom fichier ne-respecte-pas-la-norme-11.pdf ne correspond pas au nom du message métier 045-214502494-20170717-D201717-DE-1-1_0.xml"
		);

		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO','EACT');
		$archiveValidator->validate(__DIR__."/nom-fichier-annexe-failed/SLO-EACT--214502494--20170717-5.tar.gz");

	}

	/**
	 * @throws Exception
	 */
	public function testOK(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO','EACT');
        $this->assertTrue(
		    $archiveValidator->validate(__DIR__."/../SLO-EACT--SPREF0626-246201149-20170717-4675.tar.gz")
        );
	}

	/**
	 * @throws Exception
	 */
	public function testWithTypologie(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT');
		$this->assertTrue(
            $archiveValidator->validate(__DIR__."/LSC-EACT--214400330--20180910-44.tar.gz")
        );
	}

	/**
	 * @throws Exception
	 */
	public function testWithAnnexe(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO','EACT');
		$this->assertTrue(
            $archiveValidator->validate(
                __DIR__."/../FONCT-09/ok-xml/SLO-EACT--214502494--20170717-5.tar.gz"
            )
        );
	}


	/**
	 * @throws Exception
	 */
	public function testMandatoryTypologie(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT',true);
		$this->assertTrue($archiveValidator->validate(__DIR__."/LSC-EACT--214400330--20180910-44.tar.gz"));
	}

	/**
	 * @throws Exception
	 */
	public function testMandatoryTypologieFailed(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO','TACT',true);
		$this->setExpectedException(Exception::class,"Le fichier 001-000000000-20170130-TEST42-DE-1-1_1.pdf n'a pas de typologie et celle-ci est obligatoire");
		$archiveValidator->validate(__DIR__."/../../fixtures/SLO-TACT--000000000--20170201-001.tar.gz");
	}

}