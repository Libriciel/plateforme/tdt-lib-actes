<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme09Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }


    public function testMissingFile(){
        $this->setExpectedException(
            "Exception",
            "Le fichier 045-214502494-20170717-D201717-DE-1-1_1.pdf est référencé, mais il n'est pas présent dans l'archive"
        );
        $this->archive->getArchiveData(__DIR__."/missing-files/EACT--214502494--20170717-5.xml");
    }

    public function testSirenMismatch(){
        $this->setExpectedException(
            "Exception",
            "Le numéro SIREN du nom du fichier 214502494 diffère de celui trouvé dans l'enveloppe métier 000000000"
        );
        $this->archive->getArchiveData(__DIR__."/siren-mismatch/EACT--214502494--20170717-5.xml");
    }

}