<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme08Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }

    public function testSameApplication(){
        $archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO','EACT');
        $this->assertTrue(
            $archiveValidator->validate(__DIR__."/../SLO-EACT--SPREF0626-246201149-20170717-4675.tar.gz")
        );
    }

    public function testNotSameApplication(){
        $archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO','BALO');
        $this->setExpectedException("Exception","L'identifiant de l'application de l'archive EACT ne correspond pas à l'identifiant courant BALO");
        $archiveValidator->validate(__DIR__."/../SLO-EACT--SPREF0626-246201149-20170717-4675.tar.gz");
    }

    public function testNumActeMissing(){
        $archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SIL','EACT');
        $this->setExpectedException("Exception",
            "Le numéro interne du nom du fichier métier ()  ne correspond pas au contenu XML (AUDIT1)");
        $archiveValidator->validate(__DIR__."/../../fixtures/test-numacte-missing/SIL-EACT--268708567--20180918-1.tar.gz");
    }


}