<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme10Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }


    public function testSirenMismatch(){
        $this->setExpectedException(
            "Exception",
            "Le numéro SIREN du message métier 000000000 diffère de celui trouvé dans l'enveloppe métier 214502494"
        );
        $this->archive->getArchiveData(__DIR__."/siren-enveloppe-message-mismatch/EACT--214502494--20170717-5.xml");
    }


}