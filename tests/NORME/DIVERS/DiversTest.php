<?php

use Libriciel\LibActes\Archive;

use Libriciel\LibActes\Utils\TmpDir;

class DiversTest extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }


	/**
	 * @throws Exception
	 */
    public function testSirenMismatch(){
		$tmpDir = new TmpDir();
		$tmp_dir = $tmpDir->getTmpDir();
		$this->setExpectedException(Exception::class,"L'enveloppe ne contient pas d'emetteur");
        $this->archive->getArchiveDataFromTarball(__DIR__."/emetteur-not-found/LSC-EACT---214400330-20180823-19.tar.gz",$tmp_dir);
    }

	/**
	 * @throws Exception
	 */
    public function testNombreAnnexeMismatch(){
    	$archiveValidator  = new \Libriciel\LibActes\ArchiveValidator('SLO','TACT');
    	$this->setExpectedException(Exception::class,"Le nombre d'annexe trouvé (2) ne correspond pas à celui indiqué dans le fichier XML (4)");
    	$archiveValidator->validate(__DIR__."/../../fixtures/test-archive-nombre-mismatch/SLO-TACT--000000000--20170201-001.tar.gz");
	}

}