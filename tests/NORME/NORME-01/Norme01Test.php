<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme01Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }

    public function testOK(){
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();


        $archiveData = $this->archive->getArchiveDataFromTarball(
            __DIR__ . "/../SLO-EACT--SPREF0626-246201149-20170717-4675.tar.gz",
            $tmp_dir
        );

        $tmpDir->destruct();

        /** @var \Libriciel\LibActes\FichierXML\MessageMetierARActes $fichierXML */
        $fichierXML = $archiveData->fichierXML[0];
        $this->assertEquals('MARCHE17_45', $fichierXML->numero_interne);
    }

    public function testEnveloppeMetierNotXML(){
        try {
            $this->archive->getArchiveData(
                __DIR__ . "/bad_xml_enveloppe/EACT--SPREF0626-246201149-20170717-4675.xml"
            );
            $this->assertTrue(false);
        } catch (XSDValidationException $e){
            $this->assertStringStartsWith("Element 'toto': This element is not expected",$e->getValidationErrors()[0]->message);
        }
    }

    public function testMessageMetierNotXML(){
        $archive = new \Libriciel\LibActes\Archive();
        try {
            $archive->getArchiveData(
                __DIR__ . "/bad_xml_message_metier/EACT--SPREF0626-246201149-20170717-4675.xml"
            );
            $this->assertTrue(false);
        } catch (XSDValidationException $e){
            $this->assertStringStartsWith("Element 'toto': This element is not expected",$e->getValidationErrors()[0]->message);
        }
    }



}