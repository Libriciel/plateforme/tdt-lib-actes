<?php
use Libriciel\LibActes\ArchiveValidator;

class FONCT09Test extends PHPUnit_Framework_TestCase {

    /** @var  ArchiveValidator */
    protected $archiveValidator;

    public function setUp(): void
    {
        $this->archiveValidator = new ArchiveValidator('SLO');
    }

    public function testOK(){
        $this->assertTrue(
            $this->archiveValidator->validate(__DIR__."/ok/SLO-EACT--214502494--20170717-5.tar.gz")
        );
    }

    public function testOKXML(){
        $this->assertTrue(
            $this->archiveValidator->validate(__DIR__."/ok-xml/SLO-EACT--214502494--20170717-5.tar.gz")
        );
    }

    public function testKOXMLOnly(){
        $this->setExpectedException(
            "Exception",
            "Un document budgétaire XML doit être accompagné d'au moins une annexe au format pdf, jpeg ou png"
        );
        $this->archiveValidator->validate(__DIR__."/ko-xml-only/SLO-EACT--214502494--20170717-5.tar.gz");
    }


    public function testKONoScellement(){
        $this->setExpectedException(
            "Exception",
            "Un document budgétaire XML doit comporter une balise de scellement"
        );
        $this->archiveValidator->validate(__DIR__."/ko-no-scellement/SLO-EACT--214502494--20170717-5.tar.gz");
    }


    public function testKONoScellementInverse(){
        //Même si le XML n'est pas le document principal alors il doit contenir une balise de scellement
        //Ceci n'est pas explicite dans l'exigence, mais lié à une communication de la DGCL
        $this->setExpectedException(
            "Exception",
            "Un document budgétaire XML doit comporter une balise de scellement"
        );
        $this->archiveValidator->validate(__DIR__."/ko-no-scellement-inverse/SLO-EACT--214502494--20170717-5.tar.gz");
    }

	public function testKONoXML(){
		$this->setExpectedException(
			"Exception",
			"Le document n'est pas un document XML"
		);
		$this->archiveValidator->validate(__DIR__."/ko-no-xml/SLO-EACT--214502494--20170717-5.tar.gz");
	}



}