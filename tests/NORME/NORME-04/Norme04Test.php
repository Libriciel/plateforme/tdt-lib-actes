<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme04Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }

    public function testOK(){

        $archiveData = $this->archive->getArchiveData(
            __DIR__ . "/../ok/EACT--214502494--20170717-5.xml"
        );

        /** @var \Libriciel\LibActes\FichierXML\MessageMetierARActes $fichierXML */
        $fichierXML = $archiveData->fichierXML[0];
        $this->assertEquals('D201717', $fichierXML->numero_interne);
    }

    public function testEmptyField(){
        try {
            $archiveData = $this->archive->getArchiveData(
                __DIR__ . "/empty-fields/EACT--214502494--20170717-5.xml"
            );
            $this->assertTrue(false);
        } catch (Exception $e){
            $this->assertEquals("Le champs objet est obligatoire",$e->getMessage());
        }
    }





}