<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme06Test extends PHPUnit_Framework_TestCase {

    /** @var  Archive */
    protected $archive;

    public function setUp(): void
    {
        $this->archive = new Archive();
    }


    public function testNameMismatch(){
        $this->setExpectedException("Exception","L'archive présente au moins un fichier en trop : EACT--214502494--20170717-6.xml");
        $this->archive->getArchiveData(__DIR__."/too-many-files/EACT--214502494--20170717-5.xml");
    }



}