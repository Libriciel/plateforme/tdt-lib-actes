<?php

use Libriciel\LibActes\Utils\TmpDir;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\Utils\XSDValidationException;

class Norme13Test extends PHPUnit_Framework_TestCase {

	/** @var  Archive */
	protected $archive;

	public function setUp(): void
    {
		$this->archive = new Archive();
	}


	/**
	 * @throws Exception
	 */
	public function testNatureAndTypologieOK(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT');
		$this->assertTrue($archiveValidator->validate(__DIR__."/../NORME-11/LSC-EACT--214400330--20180910-44.tar.gz",[3],['99_AU']));
	}

	/**
	 * @throws Exception
	 */
	public function testNatureAndTypologieNoTest(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT');
		$this->assertTrue($archiveValidator->validate(__DIR__."/../NORME-11/LSC-EACT--214400330--20180910-44.tar.gz"));
	}

	/**
	 * @throws Exception
	 */
	public function testNatureAndTypologieBadNature(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT');
		$this->setExpectedException(Exception::class,'La nature 3 n\'est pas autorisé pour le message métier 034-214400330-20180910-HOMOLAUDIT44-AI-1-1_0.xml');
		$archiveValidator->validate(__DIR__."/../NORME-11/LSC-EACT--214400330--20180910-44.tar.gz",[4,1,2],['99_AU']);
	}


	/**
	 * @throws Exception
	 */
	public function testNatureAndTypologieClassification(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('abc','TACT');
		$this->assertTrue($archiveValidator->validate(__DIR__."/fixtures/abc-TACT--000000000--20181025-1.tar.gz",[4,1,2],['99_AU']));
	}

	/**
	 * @throws Exception
	 */
	public function testNatureAndTypologieBadTypologie(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT');
		$this->setExpectedException(Exception::class,'La typologie 99_AU n\'est pas permise sur le fichier 99_AU-034-214400330-20180910-HOMOLAUDIT44-AI-1-1_1.pdf');
		$archiveValidator->validate(__DIR__."/../NORME-11/LSC-EACT--214400330--20180910-44.tar.gz",[3],['11_GD']);
	}

	/**
	 * @throws Exception
	 */
	public function testNatureAndTypologieNoTypologie(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('SLO','TACT');
		$this->assertTrue($archiveValidator->validate(__DIR__."/../../fixtures/SLO-TACT--000000000--20170201-001.tar.gz",[1,2,3,4],['11_GD']));
	}

    /**
     * @throws Exception
     */
	public function testNatureAndTypologieNouvelleNotice(){
        $archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT',true);
        $archiveValidator->setValidationTypologieByNature(
            [
                3 => [
                '00_AI' => 'testAI',
                '99_AU' => 'testAI',
                ]
            ]
        );
        $this->assertTrue($archiveValidator->validate(
            __DIR__."/../NORME-11/LSC-EACT--214400330--20180910-44.tar.gz",[1,2,3,4],['99_AU'])
        );

    }

    /**
     * @throws Exception
     */
    public function testNatureAndTypologieNouvelleNoticeFailed(){
        $archiveValidator = new \Libriciel\LibActes\ArchiveValidator('LSC','EACT',true);
        $archiveValidator->setValidationTypologieByNature(
            [
                3 => [
                    '00_AI' => 'testAI',
                    '99_DE' => 'testAI',
                ]
            ]
        );
        $this->setExpectedException(
            Exception::class,
            "La typologie 99_AU n'est pas permise sur le fichier 99_AU-034-214400330-20180910-HOMOLAUDIT44-AI-1-1_1.pdf pour la nature 3"
        );
        $archiveValidator->validate(
            __DIR__."/../NORME-11/LSC-EACT--214400330--20180910-44.tar.gz",[1,2,3,4],['99_AU','99_DE']
        );

    }


}