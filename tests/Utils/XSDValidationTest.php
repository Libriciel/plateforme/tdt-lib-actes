<?php

use \Libriciel\LibActes\Utils\XSDValidation;
use \Libriciel\LibActes\ActesXSD;
use \Libriciel\LibActes\Utils\XSDValidationException;

class XSDValidationTest extends PHPUnit_Framework_TestCase {

    /** @var  ActesXSD */
    private $actesXSD;

    /** @var  XSDValidation */
    private $xsdValidation;

    protected function setUp(): void
    {
        parent::setUp();
        $this->actesXSD = new ActesXSD();
        $this->xsdValidation = new XSDValidation();
    }

    public function testValidate(){
        $this->assertTrue(
        $this->xsdValidation->validateFile(
                __DIR__."/../fixtures/TACT--000000000--20170201-001.xml",
                $this->actesXSD->getCurrentXSDPath()
            )
        );
    }

    public function testValidate2(){
        $this->assertTrue(
            $this->xsdValidation->validateFile(
                __DIR__."/../fixtures/001-000000000-20170130-TEST42-DE-1-1_0.xml",
                $this->actesXSD->getCurrentXSDPath()
            )
        );
    }

    public function testValidateFalse(){
        try {
            $this->xsdValidation->validate("<toto></toto>", $this->actesXSD->getCurrentXSDPath());
        } catch (XSDValidationException $e){
            $this->assertEquals(
                array(
                    "Level : 2 - Code : 1845 - Line : 1 - Column : 0 - Message : Element 'toto': No matching global declaration available for the validation root.\n"
                ),
                $e->displayValidationErrors()
            );
            return;
        }
        $this->assertTrue(false);
    }

}