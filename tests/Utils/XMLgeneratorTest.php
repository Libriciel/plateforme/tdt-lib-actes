<?php

use Libriciel\LibActes\ActesXSD;

class XMLgeneratorTest extends PHPUnit_Framework_TestCase {

    public function testSameNodeBegin(){

        $xmlGenerator = new \Libriciel\LibActes\Utils\XMLGenerator();
        $mapping = array(
            "actes:Acte" => array(
                    '@actes:Date'=>'2017-01-30',
                    'actes:Document' => array('actes:NomFichier'=>'foo'),
                    'actes:DocumentPapier' => 'O'

            )
        );

        $xml = $xmlGenerator->createXMLfromXSD(
            ActesXSD::getXSD(),
            $mapping);
        $this->assertNotEmpty($xml);
    }


    public function testEsperlutette(){

        $xmlGenerator = new \Libriciel\LibActes\Utils\XMLGenerator();
        $mapping = array(
            "actes:Acte" => array(
                '@actes:Date'=>'2017-01-30',
                'actes:Document' => array('actes:NomFichier'=>'foo&bar'),
                'actes:DocumentPapier' => 'O'

            )
        );

        $xml = $xmlGenerator->createXMLfromXSD(
            ActesXSD::getXSD(),
            $mapping);
        $this->assertNotEmpty($xml);
        $simplexml = simplexml_load_string($xml);
        $this->assertNotNull($simplexml);
        $this->assertEquals(
            'foo&bar',
            strval($simplexml->children(ActesXSD::ACTES_NAMESPACE)->Document->NomFichier)
        );
    }


    public function testEsperlutetteInAttribute(){

        $xmlGenerator = new \Libriciel\LibActes\Utils\XMLGenerator();
        $mapping = array(
            "actes:Acte" => array(
                '@actes:Date'=>'2017-01-30',
                '@actes:NumeroInterne' => 'foo&bar',
                'actes:Document' => array('actes:NomFichier'=>'normal'),
                'actes:DocumentPapier' => 'O'
            )
        );

        $xml = $xmlGenerator->createXMLfromXSD(
            ActesXSD::getXSD(),
            $mapping);
        $this->assertNotEmpty($xml);

        $simplexml = simplexml_load_string($xml);

        $this->assertNotNull($simplexml);
        $this->assertEquals(
            'foo&bar',
            strval($simplexml->attributes(ActesXSD::ACTES_NAMESPACE)->NumeroInterne)
        );
    }

}