<?php

use Libriciel\LibActes\EnveloppeMetierFilename;
use Libriciel\LibActes\EnveloppeMetierFilenameField;

class EnveloppeMetierFilenameTest extends PHPUnit_Framework_TestCase {

	const IDENTIFIANT_APPLI = "EACT";

	/** @var  EnveloppeMetierFilename */
	private $enveloppeMetierFilename;

	protected function setUp(): void
    {
		parent::setUp();
		$this->enveloppeMetierFilename = new EnveloppeMetierFilename();
	}

	public function testOK(){
        $enveloppeField = $this->enveloppeMetierFilename->getEnveloppeField("EACT--123456789--20021231-1.xml");
        $enveloppeFieldExpected = new EnveloppeMetierFilenameField();
        $enveloppeFieldExpected->is_ano = false;
        $enveloppeFieldExpected->appli = "EACT";
        $enveloppeFieldExpected->info_appli = "";
        $enveloppeFieldExpected->emetteur = "123456789";
        $enveloppeFieldExpected->destinataire = "";
        $enveloppeFieldExpected->date = "20021231";
        $enveloppeFieldExpected->numero = "1";
        $this->assertEquals($enveloppeFieldExpected,$enveloppeField);
		$this->enveloppeMetierFilename->getEnveloppeField("EACT--123456789--20021231-2.xml");
	}

	private function validateBadName($bad_name,$exception_message){
		$this->setExpectedException("Exception", $exception_message);
		$this->enveloppeMetierFilename->getEnveloppeField($bad_name);
	}

	public function testBadExtension(){
		$bad_name = "EACT--123456789--20021231-1.tar.gz";
		$this->validateBadName($bad_name, "Le nom du fichier enveloppe $bad_name est mal formé.");
	}
	
	public function testBadDate(){
		$bad_name = "EACT--123456789--20150229-1.xml";
		$this->validateBadName(
			$bad_name,
			"La date 20150229 trouvé dans le nom du fichier enveloppe EACT--123456789--20150229-1.xml n'existe pas"
		);
	}

	public function testValidate(){
        $this->assertTrue(
            $this->enveloppeMetierFilename->validate("EACT--123456789--20021231-1.xml")
        );
    }

    public function testIsEnveloppeFilename(){
	    $this->assertFalse($this->enveloppeMetierFilename->isEnveloppeFilename("toto"));
        $this->assertTrue($this->enveloppeMetierFilename->isEnveloppeFilename("EACT--123456789--20021231-1.xml"));
    }

    public function testIsAnoEnveloppeFilename(){
        $this->assertTrue($this->enveloppeMetierFilename->isEnveloppeFilename("ANO_EACT--210703385--20170612-2.xml"));
    }

    public function testGetAnoEnveloppeField(){
        $enveloppeField = $this->enveloppeMetierFilename->getEnveloppeField("ANO_EACT--210703385--20170612-2.xml");
        $enveloppeFieldExpected = new EnveloppeMetierFilenameField();
        $enveloppeFieldExpected->is_ano = true;
        $enveloppeFieldExpected->appli = "EACT";
        $enveloppeFieldExpected->info_appli = "";
        $enveloppeFieldExpected->emetteur = "210703385";
        $enveloppeFieldExpected->destinataire = "";
        $enveloppeFieldExpected->date = "20170612";
        $enveloppeFieldExpected->numero = "2";
        $this->assertEquals($enveloppeFieldExpected,$enveloppeField);
    }


}