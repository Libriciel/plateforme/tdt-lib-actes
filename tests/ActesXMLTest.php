<?php

use Libriciel\LibActes\ActesXML;
use PHPUnit\Framework\TestCase;

class ActesXMLTest extends TestCase {

    public function testgetDataFromXMLFailed(){

        $actesXML = new ActesXML();
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Le document n'est pas valide");
        $actesXML->getDataFromFile(__DIR__."/fixtures/toto.xml");

    }



}