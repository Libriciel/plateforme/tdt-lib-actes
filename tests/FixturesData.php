<?php

use Libriciel\LibActes\ArchiveData;

class FixturesData {

    public function getTransmissionActesData() {
        $transmissionActesData = new \Libriciel\LibActes\FichierXML\MessageMetierActes();

        $transmissionActesData->siren = "000000000";
        $transmissionActesData->actes_filepath = __DIR__."/fixtures/deliberation.pdf";
        $transmissionActesData->annexe_filepath = array(
            __DIR__."/fixtures/tuxapple.png",
            __DIR__."/fixtures/deliberation.pdf"
        );
        $transmissionActesData->classification_date_version = "2014-01-02";
        $transmissionActesData->code_nature_numerique = 1;
        $transmissionActesData->date_actes = "2017-01-30";

        $transmissionActesData->numero_interne = "TEST42";

        $transmissionActesData->departement = "001";

        $transmissionActesData->objet = "test de télétransmission";

        $transmissionActesData->classification_1 = 1;
        $transmissionActesData->classification_2 = 3;
        $transmissionActesData->classification_3 = 4;
        return $transmissionActesData;
    }


    public function getArchiveData(){

        $transmissionActesData = $this->getTransmissionActesData();

        $archiveData = new ArchiveData();
        $archiveData->is_ano = false;
        $archiveData->id_tdt = "SLO";
        $archiveData->date_generation=date("Ymd",strtotime("2017-02-01"));
        $archiveData->numero_sequentiel = "001";
        $archiveData->id_application = "TACT";

        $archiveData->departement="012";
        $archiveData->arrondissement=1;
        $archiveData->siren="000000000";
        $archiveData->nature= "01";
        $archiveData->adresses_retour = array(
            "toto@libriciel.fr",
            "titi@libriciel.fr"
        );
        $archiveData->email_referent = "foo@libricile.fr";
        $archiveData->nom_referent = "foo";
        $archiveData->telephone_referent = "00 00 00 00 00";

        $archiveData->fichierXML = array(
            $transmissionActesData
        );
        return $archiveData;
    }

}