<?php

use Libriciel\LibActes\ActesXML;
use Libriciel\LibActes\Archive;
use Libriciel\LibActes\ArchiveValidator;
use Libriciel\LibActes\Utils\TmpDir;

require_once __DIR__."/FichierXML/MessageMetierARActesTest.php";

class ArchiveTest extends PHPUnit_Framework_TestCase {

    private function getArchive(){
        return new Archive();
    }

    public function testGenerate(){

        $fixtureData = new FixturesData();
        $archiveData = $fixtureData->getArchiveData();

        $tmpDir = new TmpDir();
        $tmp_dir =$tmpDir->getTmpDir();

        $archive = $this->getArchive();
        $enveloppe_filepath = $archive->generateFiles($archiveData,$tmp_dir);

        $this->assertFileExists($enveloppe_filepath);
        $this->assertFileEquals(
            __DIR__."/fixtures/result-enveloppe-archive.xml",
            $enveloppe_filepath
        );

        $xml = simplexml_load_file($enveloppe_filepath);

        $xml->registerXPathNamespace(
            'actes',
            'http://www.interieur.gouv.fr/ACTES#v1.1-20040216'
        );

        $node = $xml->xpath("//actes:NomFichier") ;

        $this->assertFileEquals(
            __DIR__ . "/FichierXML/fixtures/001-000000000-20170130-TEST42-DE-1-1_0.xml",
            $tmp_dir."/".strval($node[0])
        );

        $tmpDir->destruct();
    }

    public function testGenerateZip(){
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $fixtureData = new FixturesData();
        $archiveData = $fixtureData->getArchiveData();

        $archive = $this->getArchive();
        $tarball_path = $archive->generateZip($archiveData,$tmp_dir);


        $this->assertFileExists($tarball_path);

        $tmp_dir_2 = $tmpDir->getTmpDir();

        //Il semble qu'il y ait un bug spécifique sur Ubuntu 16.04 avec les fonction compress/extractTo
        /*$pharData = new \PharData($tarball_path);
        $pharData->extractTo($tmp_dir_2);*/
        `tar xvzf $tarball_path -C $tmp_dir_2`;

        $this->assertFileEquals(
            __DIR__."/fixtures/tuxapple.png",
            $tmp_dir_2."/001-000000000-20170130-TEST42-DE-1-1_2.png"
        );

        $this->assertFileEquals(
            __DIR__."/fixtures/deliberation.pdf",
            $tmp_dir_2."/001-000000000-20170130-TEST42-DE-1-1_1.pdf"
        );
        $this->assertFileEquals(
            __DIR__."/fixtures/deliberation.pdf",
            $tmp_dir_2."/001-000000000-20170130-TEST42-DE-1-1_3.pdf"
        );

        $this->assertFileEquals(
            __DIR__ . "/FichierXML/fixtures/001-000000000-20170130-TEST42-DE-1-1_0.xml",
            $tmp_dir_2."/001-000000000-20170130-TEST42-DE-1-1_0.xml"
        );

        $this->assertFileEquals(
            __DIR__ . "/fixtures/TACT--000000000--20170201-001.xml",
            $tmp_dir_2."/TACT--000000000--20170201-001.xml"
        );
        $tmpDir->destruct();
    }

    public function testGetArchiveData(){
        $archive = $this->getArchive();

        $archiveData= $archive->getArchiveData(
            __DIR__."/fixtures/test-archive/TACT--000000000--20170201-001.xml"
        );
        $fixtureData = new FixturesData();
        $archiveData_expected = $fixtureData->getArchiveData();
        $archiveData_expected->departement = "001";
        unset($archiveData_expected->id_tdt);
        unset($archiveData->id_tdt);
        unset($archiveData->fichierXML[0]->actes_filepath);
        unset($archiveData_expected->fichierXML[0]->actes_filepath);
        unset($archiveData_expected->fichierXML[0]->annexe_filepath);
        unset($archiveData->fichierXML[0]->annexe_filepath);
        unset($archiveData->fichierXML[0]->nombre_annexes);
        unset($archiveData->fichierXML[0]->file_path);
        unset($archiveData_expected->fichierXML[0]->file_path);
        unset($archiveData->enveloppe_path);
        unset($archiveData_expected->enveloppe_path);
		unset($archiveData_expected->fichierXML[0]->nombre_annexes);

        $this->assertEquals($archiveData_expected,$archiveData);
    }

    public function testGetArchiveFromTarball(){
        $tarball_path= __DIR__."/fixtures/SLO-TACT--000000000--20170201-001.tar.gz";

        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();

        $archive = $this->getArchive();

        $archiveData = $archive->getArchiveDataFromTarball($tarball_path,$tmp_dir);
        $fixtureData = new FixturesData();
        $archiveData_expected = $fixtureData->getArchiveData();
        $archiveData_expected->departement = "001";

        unset($archiveData_expected->id_tdt);
        unset($archiveData->id_tdt);
        unset($archiveData->fichierXML[0]->actes_filepath);
        unset($archiveData_expected->fichierXML[0]->actes_filepath);
        unset($archiveData_expected->fichierXML[0]->annexe_filepath);
		unset($archiveData_expected->fichierXML[0]->nombre_annexes);
        unset($archiveData->fichierXML[0]->annexe_filepath);
        unset($archiveData->fichierXML[0]->nombre_annexes);
        unset($archiveData->fichierXML[0]->file_path);
        unset($archiveData_expected->fichierXML[0]->file_path);
        unset($archiveData->enveloppe_path);
        unset($archiveData_expected->enveloppe_path);

        $this->assertEquals($archiveData_expected,$archiveData);
        $tmpDir->destruct();
    }

	/**
	 * @throws Exception
	 */
	public function testGetArchiveFromTarballBadArchive(){
		$tarball_path= __DIR__."/fixtures/notExisting.tar.gz";

		$tmpDir = new TmpDir();
		$tmp_dir = $tmpDir->getTmpDir();

		$archive = $this->getArchive();
		$this->setExpectedException(Exception::class,"Erreur (2) lors de la décompression de l'archive");
		$archive->getArchiveDataFromTarball($tarball_path,$tmp_dir);

		$tmpDir->destruct();
	}


	public function testGenerateMICL(){
        $messageMetierARActesTest = new MessageMetierARActesTest();
        $messageMetierARActes = $messageMetierARActesTest->getFichierXML();

        $fixtureData = new FixturesData();
        $archiveData = $fixtureData->getArchiveData();
        $archiveData->fichierXML = [$messageMetierARActes];
        $archiveData->service_controle = "PREF001";
        $archiveData->departement_pref = "001";
        $archiveData->id_tdt = null;
        $archiveData->departement = null;
        $archiveData->arrondissement = null;
        $archiveData->nature = null;
        $archiveData->adresses_retour = array();

        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();

        $archive = new Archive();
        $archive->generateFiles($archiveData,$tmp_dir);

        $enveloppe_metier = "$tmp_dir/TACT--PREF001-000000000-20170201-001.xml";
        $this->assertFileExists($enveloppe_metier);

        $actesXML = new ActesXML();

        $enveloppeMISILLCLData = $actesXML->getDataFromXML(file_get_contents($enveloppe_metier));

        $this->assertInstanceOf("Libriciel\LibActes\FichierXML\EnveloppeMISILLCL",$enveloppeMISILLCLData);

        $archiveDataResult = $archive->getArchiveData($enveloppe_metier);

        unset($archiveData->fichierXML[0]->file_path);
        unset($archiveDataResult->fichierXML[0]->file_path);
        unset($archiveData->enveloppe_path);
        unset($archiveDataResult->enveloppe_path);
        $this->assertEquals($messageMetierARActes,$archiveDataResult->fichierXML[0]);
        $this->assertEquals($archiveData,$archiveDataResult);

        $tmpDir->destruct();
    }

    public function testNoTransmission(){
        $fixtureData = new FixturesData();
        $archiveData = $fixtureData->getArchiveData();
        $archiveData->fichierXML = [];
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();

        $archive = new Archive();
        $this->setExpectedException("Exception","Aucune transmission trouvée : impossible de déterminer le sens de la transmission");
        $archive->generateFiles($archiveData,$tmp_dir);
    }

    public function testTransmissionDeuxSens(){
        $messageMetierARActesTest = new MessageMetierARActesTest();
        $messageMetierARActes = $messageMetierARActesTest->getFichierXML();

        $messageMetierActesTest = new MessageMetierActesTest();
        $messageMetierActes = $messageMetierActesTest->getFichierXML();

        $fixtureData = new FixturesData();
        $archiveData = $fixtureData->getArchiveData();
        $archiveData->fichierXML = [$messageMetierARActes,$messageMetierActes];
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();

        $archive = new Archive();
        $this->setExpectedException("Exception","Il y a des transmissions dans les deux sens : impossible de générer l'enveloppe métier");
        $archive->generateFiles($archiveData,$tmp_dir);
    }

    public function testGetArchiveDataFromFolder(){
        $archive = new Archive();
        $archiveData = $archive->getArchiveDataFromFolder(__DIR__."/fixtures/test-archive/");
        $fixtureData = new FixturesData();
        $archiveData_expected = $fixtureData->getArchiveData();
        $archiveData_expected->departement = "001";
        unset($archiveData_expected->id_tdt);
        unset($archiveData->id_tdt);
        unset($archiveData->fichierXML[0]->actes_filepath);
        unset($archiveData_expected->fichierXML[0]->actes_filepath);
        unset($archiveData_expected->fichierXML[0]->annexe_filepath);
        unset($archiveData->fichierXML[0]->annexe_filepath);
        unset($archiveData->fichierXML[0]->nombre_annexes);
        unset($archiveData_expected->fichierXML[0]->file_path);
        unset($archiveData->fichierXML[0]->file_path);
        unset($archiveData->enveloppe_path);
        unset($archiveData_expected->enveloppe_path);
		unset($archiveData_expected->fichierXML[0]->nombre_annexes);
        $this->assertEquals($archiveData_expected,$archiveData);
    }

    public function testGetArchiveDataFromFolderBadArchive(){
        $folder = __DIR__."/fixtures/test-bad-archive/";
        $archive = new Archive();
        $this->setExpectedException("Exception","Aucun fichier de type enveloppe métier n'a été trouvé dans le répertoire $folder");
        $archive->getArchiveDataFromFolder($folder);
    }

    public function testGetArchiveDataFromFolderFichierEnTrop(){
        $folder = __DIR__."/fixtures/test-fichier-en-trop";
        $archive = new Archive();
        //$this->setExpectedException("Exception","Aucun fichier de type enveloppe métier n'a été trouvé dans le répertoire $folder");
        $archiveData = $archive->getArchiveDataFromFolder($folder);
        $this->assertNotNull($archiveData);

    }

    public function testGetArchiveDataFromFolderBadFile(){
        //Les fichier autre que PNG/XML/PDF/JPEG sont interdit que dans le sens CL -> MISIL
        $folder = __DIR__."/fixtures/test-archive-MISILCL";
        $archive = new Archive();
        $archiveData = $archive->getArchiveDataFromFolder($folder);
        $this->assertNotNull($archiveData);
    }

    public function testGetAnomalie(){
        $archive = new Archive();
        $archiveData = $archive->getArchiveDataFromFolder(__DIR__."/fixtures/test-anomalie/");
        $this->assertEquals(
            realpath(__DIR__."/fixtures/test-anomalie/ANO_EACT--210703385--20170612-2.xml"),
            realpath($archiveData->enveloppe_path)
        );
        $this->assertTrue($archiveData->is_ano);
    }

    public function testGenerateReponse(){
        $archive = new Archive();
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $archive->generateReponse(__DIR__."/fixtures/test-archive/",$tmp_dir);

        $archiveData = $archive->getArchiveDataFromFolder($tmp_dir);

        /** @var \Libriciel\LibActes\FichierXML\MessageMetierARActes $ar */
        $ar = $archiveData->fichierXML[0];
        $this->assertEquals("001-000000000-20170130-TEST42-DE-1-1_1.pdf",$ar->actes_filepath);

        $this->assertInstanceOf("Libriciel\LibActes\FichierXML\MessageMetierARActes",$archiveData->fichierXML[0]);
        $this->assertEquals("TEST42",$archiveData->fichierXML[0]->numero_interne);
    }

    public function testGenerateClassification(){
        $archive = new Archive();
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $archive->generateReponse(__DIR__."/fixtures/test-classification/",$tmp_dir);

        $archiveData = $archive->getArchiveDataFromFolder($tmp_dir);

        $this->assertInstanceOf("Libriciel\LibActes\FichierXML\MessageMetierRetourClassification",$archiveData->fichierXML[0]);
        $this->assertFileEquals(__DIR__."/../src/classification/classification.xml",$archiveData->fichierXML[0]->file_path);
    }

    /**
     * @throws \Exception
     */
    public function testGenerateReponsFromTarball(){
        $archive = new Archive();
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $archive->generateReponseFromTarball(__DIR__."/fixtures/SLO-TACT--000000000--20170201-001.tar.gz",$tmp_dir);
        $archiveData = $archive->getArchiveDataFromFolder($tmp_dir);
        $this->assertInstanceOf("Libriciel\LibActes\FichierXML\MessageMetierARActes",$archiveData->fichierXML[0]);
        $this->assertEquals("TEST42",$archiveData->fichierXML[0]->numero_interne);
    }

    // Pour les message qui ne génère pas d'AR ou de réponse
    public function testGenerateReponseFromOtherMessage(){
        $archive = new Archive();
        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $this->assertFalse($archive->generateReponse(__DIR__."/fixtures/test-archive-2-1/",$tmp_dir));
    }

    /**
     * @throws Exception
     */
    public function testGenerateArchiveWithDocxFile(){

        $archive = new Archive();

        $archiveData = $archive->getArchiveDataFromFolder(__DIR__."/fixtures/test-docx/");

        $tmpDir = new TmpDir();
        $tmp_dir = $tmpDir->getTmpDir();
        $tarball_path = $archive->generateZip($archiveData,$tmp_dir);

		$output_dir = $tmpDir->getTmpDir();
        exec("tar xvzf $tarball_path -C $output_dir");
        //$pharData = new \PharData($tarball_path);
        //$pharData->extractTo($output_dir);

        $this->assertTrue(in_array('001-000000000-20170130-TEST42-DE-1-1_1.docx',scandir($output_dir)));
    }


	/**
	 * @throws Exception
	 */
    public function testGetArchiveDataFromFolderBigNumber(){
		$archive = new Archive();
		$a = $archive->getArchiveDataFromFolder(__DIR__."/fixtures/test-archive-big-num/");
		$this->assertNotEmpty($a);
	}

    /**
     * @throws Exception
     */
    public function testGetArchiveDataFromInexistingFolder(){
        $archive = new Archive();
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Le répertoire NopeNothing n'existe pas");
        $a = $archive->getArchiveDataFromFolder("NopeNothing");
    }

    /**
     * @throws \Exception
     */
    public function testArchiveWithCFU(){
        $archiveValidator = new ArchiveValidator('abc','TACT');
        $this->assertTrue(
            $archiveValidator->validate(__DIR__."/fixtures/abc-TACT--800403222--20220323-1.tar.gz")
        );
    }

    public function testArchiveWithCFUNonScelle(){
        $archiveValidator = new ArchiveValidator('abc','TACT');
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Un document budgétaire XML doit comporter une balise de scellement");
        $archiveValidator->validate(__DIR__."/fixtures/abc-TACT--800403222--20220323-3.tar.gz");
    }
}
