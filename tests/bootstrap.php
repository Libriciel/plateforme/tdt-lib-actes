<?php

use PHPUnit\Framework\TestCase;

date_default_timezone_set("Europe/Paris");
require_once(__DIR__."/../vendor/autoload.php");

require_once (__DIR__."/FixturesData.php");
require_once (__DIR__ . "/FichierXML/FichierXMLTesting.php");

class PHPUnit_Framework_TestCase extends TestCase {

    public function setExpectedException($className, $exceptionMessage) {
        $this->expectException($className);
        $this->expectExceptionMessage($exceptionMessage);
    }

};