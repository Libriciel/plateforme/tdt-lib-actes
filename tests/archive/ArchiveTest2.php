<?php

use Libriciel\LibActes\Archive;

class ArchiveTest2 extends PHPUnit_Framework_TestCase {

	/** @var  Archive */
	protected $archive;

	public function setUp(): void
    {
		$this->archive = new Archive();
	}

	/**
	 * @throws Exception
	 */
	public function testOK(){
		$archiveValidator = new \Libriciel\LibActes\ArchiveValidator('AGE','EACT');
		$this->setExpectedException(Exception::class,"La nature de l'actes du fichier métier (1) ne correspond pas au contenu XML (3)");
		$archiveValidator->validate(__DIR__."/AGE-EACT--214203101--20201109-1.tar.gz");
	}
}
