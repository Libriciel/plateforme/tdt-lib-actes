## Version 2.0.4 - 27-03-2024

## Corrections
- Correction de la compatibilité PHP 8.1

## Version 2.0.3 - 16-05-2023

## Corrections
- Adaptation du xsd aux enveloppes émises par la DGCL

## Version 2.0.2 - 13-03-2023

## Corrections
- Mise à jour de la classification

## Version 2.0.1

## Corrections
- Correction d'un bug lorsque le répertoire à analyser est vide
- Mise à jour de la classification

## Version 2.0 - 2022-06-07

## Évolution
- Passage en PHP 8.1 / Ubuntu 22.04

## Version 1.4 - 2020-03-24

## Évolution
- Vérification du scellement des flux CompteFinancierUnique

## Version 1.3

## Évolution

- Mise à jour phpunit
- Mise à jour Dockerfile et CI

## Version 1.2.2 -

## Correction

- Mise à jour de la classification

## Version 1.2.1 - 2020-11-16

## Correction

- Correction du fichier composer pour intégration sur pakagist

## Version 1.2.0 - 2020-11-16

## Evolution

- mise à jour de la licence en AGPL V3

## Ajout

- Ajout d'une étape sonarqube pour la CI

## Version 1.1.2

## Correction

- Vérification de la cohérence de la nature de l'acte entre le nom du fichier métier et le contenu XML du fichier 

## Version 1.1.1

## Evolution

- mise à jour de la classification suivant les nouvelles directive de la DGCL

## Version 1.1.0

## Evolution

- Ajout de la fonction ArchiveValidator::setValidationTypologieByNature permettant de traiter la nouvelle notice des actes 2.2 
et de vérifier la typologie des pièces en fonctions de la nature.

## Version 1.0.9

## Evolution

- La classe de validation permet de rendre obligatoire la typologie des pièces
- Ajout de la possibilité de valider les noms de fichiers métier avec une liste de nature et une liste de typologie

## Version 1.0.8

## Evolution

- Validation de la correspondance du numéro interne de l'acte entre le XML et le nom du message métier

## Version 1.0.7

## Evolution

- Vérification du nom des fichiers lié au message métier


## Version 1.0.6

## Evolution

- Vérification de l'identifiant de l'application (quadrigramme) avec EACT par défaut

## Correction

- on vérifie que l'emetteur n'est pas vide
- on vérifier que le nombre d'annexe indiqué est identique au nombre trouvé 

## Version 1.0.5

## Evolution

- Modification de la classification pour prendre en compte la typologie de document 99_SE

## Version 1.0.4

## Correction

- remplacement de phar par la commande tar

## Version 1.0.3

## Evolution

- La DGCL envoi des enveloppe avec un numéro journalier de plus de 4 chiffres

## Version 1.0.2

## Correction

- le caractère & n'était pas échapé correctement

## Version 1.0.1

## Evolution

- on vérifie la balise de scellement y compris si elle est en annexes

## Version 1.0.0

### Ajouts

- Génération des fichiers du protocoles Actes
