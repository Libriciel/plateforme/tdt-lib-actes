[![Minimum PHP Version](http://img.shields.io/badge/php-%207.2-8892BF.svg)](https://php.net/)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
[![pipeline status](https://gitlab.libriciel.fr/pastell/pastell/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/pastell/pastell/commits/master)
[![coverage report](https://gitlab.libriciel.fr/s2low/tdt-lib-actes/badges/master/coverage.svg)](https://gitlab.libriciel.fr/s2low/tdt-lib-actes/commits/master)

La bibliothèque tdt-lib-actes contient les fonctions permettant de gérer les fichiers issus du protocole @CTES.

# Installation

```
composer install
```

## Test unitaire

```
composer test
```

Test avec couverture de code :

```
composer test-cover
```

## Limitations

- nom des enveloppe d'anomalie non géré

