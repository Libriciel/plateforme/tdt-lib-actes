#!/bin/bash

apt-get update

apt-get install -y \
  git \
  zlib1g-dev \
  zip

pecl install xdebug pcov

docker-php-ext-install \
  zip

docker-php-ext-enable \
  pcov \
  xdebug

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/local/bin
mv /usr/local/bin/composer.phar /usr/local/bin/composer
php -r "unlink('composer-setup.php');"
