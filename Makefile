PHPUNIT=vendor/bin/phpunit
PHPUNIT_COVERAGE_ARGS=--coverage-text --colors=never --coverage-clover coverage/coverage.xml --log-junit coverage/logfile.xml

COMPOSER=composer

.DEFAULT_GOAL := help

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


.PHONY: phpunit
test: vendor ## Run PHPUnit
	$(PHPUNIT)

.PHONY: coverage
coverage: vendor ## Run PHPUnit with code coverage
	 $(PHPUNIT) $(PHPUNIT_COVERAGE_ARGS)

clean: ## Cleaning all but vendor directory
	rm -f .phpunit.result.cache
	rm -rf coverage

mrproper: clean ## Cleaning all and vendor directory
	rm -rf vendor/

install: composer.json composer.lock ## Install composer dependencies
	$(COMPOSER) install
